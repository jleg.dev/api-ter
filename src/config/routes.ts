
import { Request, Response } from "express";

import {LibraryController} from "../controller/libraryController";
import {ItemController} from "../controller/itemController";
import {CategoryController} from "../controller/categoryController";
import {LocationController} from "../controller/locationController";
import {ExemplaryController} from "../controller/exemplaryController";
import {MediaController} from "../controller/mediaController";
import {LogginController} from "../controller/logginController";
import path from "path";
const passport    = require('passport');

require('../utils/passport');

const multer = require('multer');
const upload = multer({dest: ''+process.env.DIRECTORY_PUBLIC+'/'+process.env.DIRECTORY_IMAGES+''});


export class Routes {
    public libraryController: LibraryController = new LibraryController();
    public logginController: LogginController = new LogginController();
    public itemController: ItemController = new ItemController();
    public categoryController: CategoryController = new CategoryController();
    public locationController: LocationController = new LocationController();
    public exemplaryController: ExemplaryController = new ExemplaryController();
    public mediaController: MediaController = new MediaController();

    public routes(app: any): void {
        // app.use(upload.array());

        app.post('/',
            function(req, res) {
                res.json({
                    msg: "Welcome to Libray API ! "
                })
            }
        );

        app.get('/api',
            function(req, res) {
                res.setHeader("Content-Type", "text/html");
                console.log("yes");
                res.sendFile(path.join(__dirname,'../../public/doc','index.html'));
            }
        );

        //                                              ### Free ###
        app.post('/register', this.logginController.register);
        app.post('/login', this.logginController.login);


        //                                              ### Limited token ###
        app.post('/library', passport.authenticate('jwt_limited', {session: false}), this.libraryController.insert);
        // Library
        app.get('/library_list', passport.authenticate('jwt_limited', {session: false}), this.libraryController.list);
        app.post('/library_connect/:library_id', passport.authenticate('jwt_limited', {session: false}), this.logginController.connectLibrary);
        app.post('/library_join', passport.authenticate('jwt_limited', {session: false}), this.logginController.joinLibrary);

        // Media
        app.post('/media/upload_image', passport.authenticate('jwt_limited', {session: false}),upload.single('img'), this.mediaController.uploadImage);


        //                                              ### Full token ###
        app.use(passport.authenticate('jwt_full', {session: false}));
        // Item
        app.get('/item', this.itemController.list);
        app.post('/item', this.itemController.insert);
        app.get('/item/:item_id', this.itemController.show)
        app.put('/item/:item_id', this.itemController.update);
        app.get('/item/:item_id/exemplary', this.itemController.listExemplaries);
        app.delete('/item/:item_id', this.itemController.delete);
        // Category
        app.get('/category', this.categoryController.list);
        app.post('/category', this.categoryController.insert);
        app.get('/category/:category_id', this.categoryController.show)
        app.put('/category/:category_id', this.categoryController.update);
        app.delete('/category/:category_id', this.categoryController.delete);
        // Location
        app.get('/location', this.locationController.list);
        app.post('/location', this.locationController.insert);
        app.get('/location/:location_id', this.locationController.show)
        app.put('/location/:location_id', this.locationController.update);
        app.get('/location/:location_id/exemplary', this.locationController.listExemplaries)
        app.delete('/location/:location_id', this.locationController.delete);
        // Exemplary
        app.get('/exemplary', this.exemplaryController.list);
        app.post('/exemplary', this.exemplaryController.insert);
        app.get('/exemplary/:exemplary_id', this.exemplaryController.show)
        app.put('/exemplary/:exemplary_id', this.exemplaryController.update);
        app.delete('/exemplary/:exemplary_id', this.exemplaryController.delete);
        app.post('/exemplary/:exemplary_id/lend', this.exemplaryController.lend);
        app.post('/exemplary/:exemplary_id/recover', this.exemplaryController.recovery);
        app.post('/exemplary/:exemplary_id/move', this.exemplaryController.move);
        // Library
        app.get('/library_users_list', this.libraryController.listUsers);
        app.put('/library', this.libraryController.update);
    }
}
