if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({path:'./.env'});
}

export const mongoose = require('mongoose');

const db_utils = require('./utils/dbUtils');
import app from "./app";
const PORT = process.env.PORT || 8080;
process.env.NODE_ENV == 'development'? process.env.URL_DEV = process.env.URL_DEV + ":" + PORT : null;

module.exports = {
    mongoose: mongoose
};


app.listen(PORT, () => {
    console.clear();
    console.log(`Server.ts : Server is litenning to port : ${PORT}!`);
    db_utils.try_connection();
});




