import * as express_imported from "express";
import * as bodyParser from "body-parser";

import { Routes } from "./config/routes";

const express = require('express');
class App {
    public app: express_imported.Application;
    public routePrv: Routes = new Routes();
    private static appInstance: App;

    private constructor() {
        this.app = express();
        this.config();
        this.routePrv.routes(this.app);
    }

    private config(): void {
        // for parsing application/json
        this.app.use(bodyParser.json());
        // for parsing application/xwww-form-urlencoded
        this.app.use(bodyParser.urlencoded({ extended: false }));


        this.app.use('/'+process.env.PATH_DIRECTORY_STATIC, express.static(process.env.DIRECTORY_PUBLIC));
        this.app.use(express.static(process.env.DIRECTORY_PUBLIC+'/doc'));
        // for setting header globally
        this.app.use(function(req, res, next) {
            res.setHeader("Content-Type", "application/json");

            return next();
        });
        // for getting more clear error
        // @ts-ignore
        this.app.use(function (err: any, req: Request, res: Response, next: any) {
            if(process.env.NODE_ENV != 'production') {
                console.log('This is the invalid field ->', err.field)
            }
            next(err)
        })
        // For setting a static directory using to serve public file
        this.app.use(express.static('public/images'));
    }

    public static getInstance(){
        if(!this.appInstance){
            this.appInstance = new App();
        }
        return this.appInstance;
    }
}


export default App.getInstance().app;
