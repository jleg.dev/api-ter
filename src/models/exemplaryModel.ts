import {mongoose} from "../server";
let LocationModel = require('../models/locationModel').LocationModel;
let ItemModel = require('../models/itemModel').ItemModel;
let ExemplarySchema = new mongoose.Schema({
        item: {type: mongoose.ObjectId, required: true, ref: 'Item', autopopulate: true},
        label: {type: String, default:null},
        location: {type: mongoose.ObjectId, required: true, ref: 'Location', autopopulate: true},
        modified: {type: Date, default: Date.now()},
        created: {type: Date, default: Date.now()},
        lend_information: {type: Object},
        library_id: {type: mongoose.Types.ObjectId, required: true}
    },
    { collection: 'Exemplary', versionKey: false}
);


ExemplarySchema.methods = {
    dynamicUpdate: function (new_values: any) {
        let current_exemplary = this;
        return new Promise((resolve, reject) => {

            if(new_values.library_id){
                delete new_values.library_id;
            }
            if (new_values.location) {
                delete new_values.location;
            }
            if (new_values.item) {
                delete new_values.item;
            }
            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }
            let property_schema = Object.getOwnPropertyNames(ExemplarySchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_exemplary[option] = new_values[option];
                }
            });
            current_exemplary.modified = Date.now();

            current_exemplary.save()
                .then(()=>{
                    return resolve(current_exemplary);
                })
                .catch(error => {
                    return reject(error);
                })
        });
    },
    move: function (new_location, library_id) {
        return new Promise((resolve, reject) => {
            let current_exemplary = this;
            let previous_location = current_exemplary.location._id;
            if (previous_location == new_location) {
                return reject({
                    error: 'current location_id is the same that new_location',
                    exemplary: current_exemplary
                });
            }
            current_exemplary.location = new_location;
            current_exemplary.save()
                .then(()=>{
                LocationModel.locationAddExemplary(current_exemplary, new_location)
                    .then(()=>{
                        LocationModel.removeExemplary(current_exemplary, previous_location, library_id)
                            .then(()=>{
                                return resolve(current_exemplary)})
                            .catch(error=>{
                                return reject(error)
                            });
                    })
                    .catch(error=>console.log(error));
            })
        })
    }
}

// @ts-ignore
ExemplarySchema.statics = {
    // @ts-ignore
    quantityDocuments: function (filters = null, keyword = null, library_id) {
        return new Promise((resolve, reject) => {
            let query = ExemplaryModel.countDocuments({library_id:library_id});
            if(filters != null){
                let properties_filter = Object.getOwnPropertyNames(filters);
                let properties_schema = Object.getOwnPropertyNames(ExemplarySchema.paths);
                properties_filter.forEach(property_filter => {
                    if(!properties_schema.includes(property_filter)){
                        // @ts-ignore
                        delete filters[property_filter];
                    }
                    query.where(filters);
                })
            }
            if (keyword) {
                query.where({$or: [
                    {"label": {"$regex": keyword, "$options": "i"}}
                    ]});
            }
            query.exec()
                .then(nbDocuments => {
                    return resolve(nbDocuments)
                })
                .catch(error => {
                    return reject(error);
                })
        })
    },
    retrieveExemplaries: function (limit: number, skip: number, filters = null, keyword = null, lean = true, library_id) {
        return new Promise((resolve, reject) => {
            let query = ExemplaryModel.find({library_id:library_id});
            if(filters != null){
                let properties_filter = Object.getOwnPropertyNames(filters);
                let properties_schema = Object.getOwnPropertyNames(ExemplarySchema.paths);
                properties_filter.forEach(property_filter => {
                    if(!properties_schema.includes(property_filter)){
                        // @ts-ignore
                        delete filters[property_filter];
                    }
                    query.where(filters);
                })
            }
            if (keyword) {
                query.where({
                    $or: [
                        {"label": {"$regex": keyword, "$options": "i"}},
                    ]
                })
            }
            query
                .limit(limit)
                .skip(skip);
                if(lean){
                    query.lean({autopopulate: true});
                }
                query
                .exec()
                .then(exemplaries => {
                    return resolve(exemplaries)
                })
                .catch(error => {
                    return reject(error);
                })
        });
    },
    // @ts-ignore
    lend: function(exemplary_id: mongoose.types.ObjectId, receiver_name:String, note:String, date: Number, library_id: mongoose.types.ObjectId){
        return new Promise((resolve, reject) => {
            let lend_information = {'receiver_name':receiver_name, 'note':note, 'date':date}
            ExemplaryModel.findOne({'_id':exemplary_id, library_id:library_id})
                .then(exemplary => {
                    if(exemplary) {
                        if (exemplary.lend_information) {
                            return reject({error: 'This exemplary is already lend',exemplary:exemplary});
                        }
                        exemplary.lend_information = lend_information;
                        exemplary.save();
                        return resolve(exemplary);
                    }
                    else{
                        return reject({error: 'This exemplary not exist.'})
                    }
                })
                .catch(error => {
                    return reject({error:error});
                })
        });
    },
    // @ts-ignore
    recovery: function(exemplary_id: mongoose.types.ObjectId, location_id?: mongoose.types.ObjectId = undefined,library_id: mongoose.types.ObjectId){
        return new Promise((resolve, reject) => {
            ExemplaryModel.findOne({'_id':exemplary_id, library_id:library_id})
                .then(exemplary => {
                    if(exemplary) {
                        if (!exemplary.lend_information) {
                            return reject({error: 'This exemplary is not actually lend',exemplary:exemplary});
                        }
                        if(location_id != undefined && exemplary.location != location_id){
                            exemplary.location = location_id;
                        }
                        exemplary.lend_information = undefined;
                        exemplary.save();
                        return resolve(exemplary);
                    }
                    else{
                        return reject({error: 'This exemplary not exist.'})
                    }
                })
                .catch(error => {
                    return reject({error:error});
                })
        });
    },
    // @ts-ignore
    delete: function(exemplary_id, library_id: mongoose.types.ObjectId){
        return new Promise((resolve, reject) => {
        ExemplaryModel.findOneAndDelete({'_id': exemplary_id, library_id:library_id})
            .then(exemplary => {
                if(exemplary){
                    LocationModel.removeExemplary(exemplary, exemplary.location, exemplary.library_id)
                        .then(()=>{
                            ItemModel.removeExemplary(exemplary, exemplary.library_id)
                                .then(()=>{
                                    return resolve(true);
                                })
                                .catch(error=>{
                                    return reject(error);
                                })
                        })
                        .catch(error=> {
                            return reject(error);
                        });
                }
                else {
                    return reject('exemplary not found');
                }
            })
            .catch((error: any) => {
                return reject([error]);
            });
        })
    },
    updateQuantity(exemplary,location){
        ItemModel.itemAddExemplary(exemplary);
        LocationModel.locationAddExemplarySync(exemplary, location);
    }
}
ExemplarySchema.plugin(require('mongoose-autopopulate'));

let ExemplaryModel = mongoose.model('Exemplary', ExemplarySchema);


// function ItemAddEx(exemplary, location){
//     ItemModel.addExemplary(exemplary, location)
//         .then(()=>{
//             console.log("ExemplaryModel : Exemplary added to Item");
//         })
//         .catch(error => {
//             console.log("Exemplarymode : ",error);
//         })
//
// }
//
// function LocationAddEx(exemplary, location){
//     LocationModel.addExemplary(exemplary, location)
//         .then(()=>{
//             console.log("ExemplaryModel : Exemplary added to LOCATION");
//         })
//         .catch(error => {
//             console.log("Exemplarymodel : ",error);
//         })
// }
// // const changeStream = ExemplaryModel.watch().on('change', change => {
// //     switch(change.operationType){
// //         case 'insert':
// //             ItemAddEx(change.fullDocument, change.fullDocument.location);
// //             LocationAddEx(change.fullDocument, change.fullDocument.location);
// //     }
// // });


exports.ExemplaryModel = ExemplaryModel;
