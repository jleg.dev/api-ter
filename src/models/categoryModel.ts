import {mongoose} from "../server";
import common from "mocha/lib/interfaces/common";


let CategorySchema = new mongoose.Schema({
        label: {type: String, required: true},
        modified: {type: Date, default: Date.now()},
        created: {type: Date, default: Date.now()},
        library_id: {type: mongoose.Types.ObjectId, required: true}
    },
    { collection: 'Category', versionKey: false }
);


CategorySchema.methods = {
    dynamicUpdate: function(new_values: any) {
        let current_category = this;
        return new Promise((resolve, reject) => {

            let property_schema = Object.getOwnPropertyNames(CategorySchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            if(new_values.library_id){
                delete new_values.library_id;
            }

            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_category[option] = new_values[option];
                }
            });
            current_category.modified = Date.now();

            current_category.save()
                .then(()=>{
                    resolve(this);
                })
                .catch(error => {
                    return reject(error);
                });
        });
    }
}

CategorySchema.statics = {
    quantityDocuments: function(keyword = null, library_id){
        return new Promise((resolve, reject) => {
            let query = CategoryModel.countDocuments({library_id:library_id});
            if(keyword){
                query.where({
                    $or: [
                        {"label": { "$regex": keyword, "$options": "i" } },
                    ]});
            }
            query.exec()
                .then(nbDocuments => {
                    return resolve(nbDocuments)})
                .catch(error => {
                    return reject(error);
                })
        })
    },
    retrieveCategories: function(limit, skip, library_id, keyword = null){
        return new Promise((resolve, reject) => {
            let query = CategoryModel.find({library_id:library_id})
            if(keyword){
                query.where({
                    $or: [
                        {"label": { "$regex": keyword, "$options": "i" } },
                    ]
                })
            }
            query
                .limit(limit)
                .skip(skip)
                .lean(true)
                .then(categories => {
                    return resolve(categories)})
                .catch(error => {
                    return reject(error);
                })
        })
    }
}

let CategoryModel = mongoose.model('Category', CategorySchema);

exports.CategoryModel = CategoryModel;
