import {mongoose} from "../server";



let LocationSchema = new mongoose.Schema({
        label: {type: String, required: true},
        quantity_max: {type: Number, default: -1},
        total_exemplaries: {type: Number, default: 0},
        total_items: {type: Number, default:0},
        modified: {type: Date, default: Date.now()},
        created: {type: Date, default: Date.now()},
        library_id: {type: mongoose.Types.ObjectId, required: true}
    },
    { collection: 'Location', versionKey: false, id:false}
);

LocationSchema.methods = {
    dynamicUpdate: function(new_values: any) {
        let current_location = this;
        return new Promise((resolve, reject) => {

            if(new_values.library_id){
                delete new_values.library_id;
            }

            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }

            let property_schema = Object.getOwnPropertyNames(LocationSchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_location[option] = new_values[option];
                }
            });
            current_location.modified = Date.now();

            current_location.save()
                .then(()=>{
                    return resolve(current_location)
                })
                .catch(error => {
                    reject(error.message);
                })
        });
    }
}




LocationSchema.statics = {
    updateQuantity: function updateQuantity(location_id: any, value: Number,library_id){
        return new Promise((resolve, reject) => {
            mongoose.model('Location', LocationSchema).updateOne(
                {"_id": location_id,library_id:library_id},
                {"$inc": {"quantity_exemplary": value}},
                {"upsert": true})
                .then(result => {
                    return resolve(result);
                })
                .catch(error => {
                    return reject(error);
                });
        });
    },
    quantityDocuments: function (filters = null, keyword = null,library_id) {
        return new Promise((resolve, reject) => {
            let query = LocationModel.countDocuments({library_id:library_id});
            if(filters != null){
                let properties_filter = Object.getOwnPropertyNames(filters);
                let properties_schema = Object.getOwnPropertyNames(LocationSchema.paths);
                properties_filter.forEach(property_filter => {
                    if(!properties_schema.includes(property_filter)){
                        // @ts-ignore
                        delete filters[property_filter];
                    }
                    query.where(filters);
                })
            }
            if (keyword) {
                query.where({$or: [
                        {"label": {"$regex": keyword, "$options": "i"}}
                    ]});
            }
            query.exec()
                .then(nbDocuments => {
                    return resolve(nbDocuments)
                })
                .catch(error => {
                    return reject(error);
                })
        })
    },
    retrieveLocations: function (limit: number, skip: number, filters = null, keyword = null,library_id) {
        return new Promise((resolve, reject) => {
            let query = LocationModel.find({library_id:library_id});
            if(filters != null){
                let properties_filter = Object.getOwnPropertyNames(filters);
                let properties_schema = Object.getOwnPropertyNames(LocationSchema.paths);
                properties_filter.forEach(property_filter => {
                    if(!properties_schema.includes(property_filter)){
                        // @ts-ignore
                        delete filters[property_filter];
                    }
                    query.where(filters);
                })
            }
            if (keyword) {
                query.where({
                    $or: [
                        {"label": {"$regex": keyword, "$options": "i"}},
                    ]
                })
            }
            query
                .limit(limit)
                .skip(skip)
                .lean({ autopopulate: true })
                .exec()
                .then(locations => {
                    return resolve(locations)
                })
                .catch(error => {
                    return reject(error);
                })
        })
    },
    locationAddExemplarySync: function(exemplary, location_id){
            LocationModel.db.model('Exemplary').countDocuments({'location': location_id, 'item': exemplary.item, 'library_id':exemplary.library_id})
                .exec()
                .then(result => {
                    let inc = {total_exemplaries: 1, total_items: 0}
                    if (result == 1) {
                        inc.total_items = 1;
                    }
                    LocationModel.updateOne(
                        {"_id": location_id,"library_id":exemplary.library_id},
                        {"$inc": inc})
                })
                .catch(error =>  {
                    console.log("Locationmodel : countDocuments : ",error);
                })
    },
    locationAddExemplary: function(exemplary, location_id){
        return new Promise((resolve, reject) => {
            LocationModel.db.model('Exemplary').countDocuments({
                'location': location_id,
                'item': exemplary.item,
                'library_id': exemplary.library_id
            })
                .exec()
                .then(result => {
                    let inc = {total_exemplaries: 1, total_items: 0}
                    if (result == 1) {
                        inc.total_items = 1;
                    }
                    LocationModel.updateOne(
                        {"_id": location_id, "library_id": exemplary.library_id},
                        {"$inc": inc})
                        .then(()=>{return resolve(true);})
                        .catch(error=>{return reject(error)});
                })
                .catch(error => {
                    console.log("Locationmodel : countDocuments : ", error);
                })
        })
    },
    removeExemplary: function(exemplary, location_id,library_id){
        return new Promise((resolve, reject) => {
            LocationModel.db.model('Exemplary').countDocuments({'location': location_id, 'item': exemplary.item,library_id:library_id}).exec(function (error, result) {
                if(!error) {
                    let inc = {total_exemplaries: -1, total_items: 0}
                    if (result == 0 ) {
                        inc.total_items = -1;
                    }
                    LocationModel.updateOne(
                        {"_id": location_id,library_id:library_id},
                        {"$inc": inc})
                        .then(() =>{
                            return resolve(true);
                        })
                        .catch(error => {
                            return reject(error);
                        })
                }
                else{
                    return reject(error);
                }
            })
        })
    }
}



let LocationModel = mongoose.model('Location', LocationSchema);

exports.LocationModel = LocationModel;
