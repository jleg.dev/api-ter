let mongoose = require('mongoose');

let ItemSchema = new mongoose.Schema({
        label: {type: String, required: true},
        author: {type: String,  default: null},
        editor: {type: String,  default: null},
        type: {type: String,  default: null},
        date_of_publish: {type: String, default: null},
        isbn: {type: String},
        category: {type: mongoose.ObjectId, ref: 'Category', autopopulate:{select:'label _id'}},
        total_exemplaries: {type: Number, default:0},
        modified: {type: Date, default: Date.now },
        created: {type: Date, default: Date.now },
        path_img: {type: String, default: null},
        library_id: {type: mongoose.Types.ObjectId, required: true}
    },
    { collection: 'Item', versionKey: false, toJSON:{virtuals: true}, id:false, toObject:{ virtuals:true }   }
);


ItemSchema.methods = {
    dynamicUpdate: function(new_values: any) {
        let current_item = this;
        return new Promise((resolve, reject) => {

            let property_schema = Object.getOwnPropertyNames(ItemSchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            if(new_values.library_id){
                delete new_values.library_id;
            }
            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }
            if(new_values.category === null || new_values.category === ""){
                new_values.category = null;
            }

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_item[option] = new_values[option];
                }
            });
            current_item.modified = Date.now();

            current_item.save()
                .then(()=>{
                    return resolve(current_item);
                })
                .catch(error =>  {
                    return reject(error);
                })
        });
    }
}

ItemSchema.statics = {
    quantityDocuments: function(keyword = null,attr = null, value = null, library_id){
        return new Promise((resolve, reject) => {
            let query = ItemModel.countDocuments({library_id:library_id});
            if(keyword){
                query.where({
                    $or: [
                        {"author": { "$regex": keyword, "$options": "i" } },
                        {"editor": { "$regex": keyword, "$options": "i" } },
                        {"date_of_publish": { "$regex": keyword, "$options": "i" } },
                        {"label": { "$regex": keyword, "$options": "i" } },
                    ]});
            }
            if(attr != null && value != null){
                // @ts-ignore
                let tab_attributes = attr.split(',');
                // @ts-ignore
                let tab_values = value.split(',');
                let itemAttr = Object.getOwnPropertyNames(ItemSchema.paths);

                for( let i = 0 ; i < tab_attributes.length ; i ++ ){
                    if(itemAttr.includes(tab_attributes[i])){
                        let query_obj = {};
                        query_obj[tab_attributes[i]] = tab_values[i];
                        query.where(query_obj);
                    }
                }
            }
            query.exec()
                .then(nbDocuments => {
                    return resolve(nbDocuments)})
                .catch(error => {
                    return reject(error);
                })
        })
    },
    retrieveItem: function(limit, skip, keyword = null, attr = null, value = null, library_id){
        return new Promise((resolve, reject) => {
            let query = ItemModel.find({library_id:library_id})
            if(keyword){
                query.where({
                    $or: [
                        {"author": { "$regex": keyword, "$options": "i" } },
                        {"editor": { "$regex": keyword, "$options": "i" } },
                        {"date_of_publish": { "$regex": keyword, "$options": "i" } },
                        {"label": { "$regex": keyword, "$options": "i" } },
                    ]
                })
            }
            if(attr != null && value != null){
                // @ts-ignore
                let tab_attributes = attr.split(',');
                // @ts-ignore
                let tab_values = value.split(',');
                let itemAttr = Object.getOwnPropertyNames(ItemSchema.paths);

                for( let i = 0 ; i < tab_attributes.length ; i ++ ){
                    if(itemAttr.includes(tab_attributes[i])){
                        let query_obj = {};
                        query_obj[tab_attributes[i]] = tab_values[i];
                        query.where(query_obj);
                    }
                }
            }
            query
                .limit(limit)
                .skip(skip)
                .lean({ autopopulate: true })
                .then(items => {
                    return resolve(items)})
                .catch(error => {
                    return reject(error);
                })
        })
    },
    itemAddExemplary: function(exemplary){
            ItemModel.updateOne(
                {_id: exemplary.item, library_id:exemplary.library_id},
                {"$inc": {total_exemplaries: 1}})
    },
    removeExemplary: function(exemplary, library_id){
        return new Promise((resolve, reject) => {
            ItemModel.updateOne(
                {"_id": exemplary.item, library_id:library_id},
                {"$inc": {total_exemplaries: -1}})
                .then(() =>{
                    return resolve(true);
                })
                .catch(error => {
                    return reject(error);
                })
        })
    }
}

ItemSchema.plugin(require('mongoose-autopopulate'));
let ItemModel = mongoose.model('Item', ItemSchema);

exports.ItemModel =  ItemModel;
