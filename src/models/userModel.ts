import {mongoose} from "../server";
import {userConstante} from "../utils/userUtils";
let userUtils = require('../utils/userUtils'),
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

let UserSchema = new mongoose.Schema({
        first_name: {type: String, required: true},
        last_name: {type: String, required: true},
        email: {type: String, required: true, unique: true, validate: {validator:userUtils.valid_email, msg:'the actual format of email is not valid'}},
        password: {type:String, required: true, validate: {validator:userUtils.valid_password, msg:'minimum size of password not respected : '+userUtils.userConstante.PASSWORD_SIZE_MIN}},
        libraries: [{type: mongoose.Types.ObjectId, default:[], ref: 'Library', autopopulate: true}],
        modified: {type: Date, default: Date.now()},
        created: {type: Date, default: Date.now()},
        token: {type: String, default: null}
    },
    { collection: 'User', versionKey: false }
);

UserSchema.pre('save', function(next) {
    // @ts-ignore
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods = {
    dynamicUpdate: function(new_values: any) {
        let current_user = this;
        return new Promise((resolve, reject) => {

            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }

            let property_schema = Object.getOwnPropertyNames(UserSchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_user[option] = new_values[option];
                }
            });
            current_user.modified = Date.now();

            current_user.save()
                .then(()=>{
                    return resolve(current_user)
                })
                .catch(error =>{
                    reject(error.message);
                })
        });
    },
    comparePassword: function(candidatePassword) {
        let current_user = this;
        console.log(candidatePassword);
        console.log(current_user.password);
        return new Promise((resolve, reject) => {
            bcrypt.compare(candidatePassword, current_user.password, function (err, isMatch) {
                if (err){
                   return reject(err);
                }
                return resolve(true);
            });
        })
    }
}

UserSchema.statics = {
    quantityLibraries: function(user_id, keyword){
      return new Promise((resolve, reject) => {
          let query = UserModel.countDocuments({'_id':user_id});
          if(keyword){
              query.where({
                  $or: [
                      {"author": { "$regex": keyword, "$options": "i" } },
                      {"editor": { "$regex": keyword, "$options": "i" } },
                      {"date_of_publish": { "$regex": keyword, "$options": "i" } },
                      {"label": { "$regex": keyword, "$options": "i" } },
                  ]});
          }
          query.exec()
              .then(nbDocuments => {
                  return resolve(nbDocuments)})
              .catch(error => {
                  return reject(error);
              })
      })
    },
    listLibraries: function(user_id, limit, skip, keyword){
        return new Promise((resolve, reject) =>{
            let query = UserModel.findOne({'_id':user_id});
            if(keyword){
                query.where({
                    $or: [
                        {"name": { "$regex": keyword, "$options": "i" } },
                        {"address": { "$regex": keyword, "$options": "i" } }
                    ]});
            }
            query
                .limit(limit)
                .skip(skip)
                .populate('libraries')
                .select('libraries')
                .then((users: any) => {
                    return resolve(users.libraries);
                })
                .catch((error: any) => {
                    return reject(error);
                });
        })
    }
}

let UserModel = mongoose.model('User', UserSchema);

exports.UserModel = UserModel;
