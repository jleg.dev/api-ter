import {mongoose} from "../server";
let UserModel = require('../models/userModel').UserModel;
let tokenUtils = require('../utils/tokenUtils');

let LibraryWorkerSchema = new mongoose.Schema({
    worker: {type: mongoose.Types.ObjectId, ref:'User'},
    role: {type: String, required:true, default:"worker"}
});

let LibrarySchema = new mongoose.Schema({
        name: {type: String, required:true},
        address:{type: String, required: true},
        modified: {type: Date, default: Date.now()},
        created: {type: Date, default: Date.now()},
        token: {type: String, default: null},
        path_img: {type: String, default:null},
        workers: {type: [LibraryWorkerSchema], required:true}
    },
    { collection: 'Library', versionKey: false }
);



LibrarySchema.methods = {
    dynamicUpdate: function(new_values: any) {
        let current_library = this;
        return new Promise((resolve, reject) => {

            let property_schema = Object.getOwnPropertyNames(LibrarySchema.paths);
            let property_new_values = Object.getOwnPropertyNames(new_values);

            if (new_values.token) {
                delete new_values.token;
            }
            if (new_values.created) {
                delete new_values.created;
            }
            if (new_values.modified) {
                delete new_values.modified;
            }

            property_new_values.forEach(function (option) {
                if (property_schema.includes(option)) {
                    current_library[option] = new_values[option];
                }
            });
            current_library.modified = Date.now();

            current_library.save()
                .then(()=>{
                    return resolve(this);
                })
                .catch(error => {
                    return reject(error.message);
                }
            );


        });
    },
    listUsers: function(){
        let current_library = this;
        return new Promise((resolve, reject) => {
            return resolve(current_library.workers);
        })
    }
}


LibrarySchema.statics = {
    create: function(values: any){
        return new Promise((resolve, reject) => {
            let worker = {'worker':values.worker,'role':values.role};
            values.workers=[worker];
            tokenUtils.generate_token_library()
                .then(token => {
                    values.token = token;
                    let newLibrary = new LibraryModel(values);
                    newLibrary.save()
                        .then(()=>{
                            UserModel.updateOne({_id:worker.worker},{$push: {libraries: newLibrary._id}})
                                .then(()=>{
                                    return resolve(newLibrary);
                                })
                                .catch(error => {
                                    return reject(error);
                                })
                        })
                        .catch(error => {
                            return reject(error);
                        })
                })
                .catch(error => {
                    return reject(error);
                })
        })
    },
    quantityUsers: function(library_id){
        return new Promise((resolve, reject) =>{
            LibraryModel.countDocuments({_id:library_id})
                .exec()
                .then(nbDocuments => {
                    return resolve(nbDocuments)})
                .catch(error => {
                    return reject(error);
                })
        })
    },
    listUsers: function(library_id, limit, skip){
        return new Promise((resolve, reject) =>{
            LibraryModel
                .findOne({_id:library_id})
                .populate({path:"workers.worker", select:"first_name last_name email _id"})
                .limit(limit)
                .skip(skip)
                .then(users=> {
                    return resolve(users.workers);
                })
                .catch(error => {
                    return reject(error);
                })
        })
    }
}

let LibraryModel = mongoose.model('Library', LibrarySchema);

exports.LibraryModel = LibraryModel;
