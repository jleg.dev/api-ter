// @ts-ignore
import  mongoose = require('mongoose');

async function connection(){
    try {
        await mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true,useUnifiedTopology: true, dbName: process.env.DATABASE_NAME});
        return;
    }
    catch (error) {
        console.error(error);
        return;
    }
}
module.exports  = {
    try_connection: async function() {
        let attempt = 0 ;
        await connection()
            .then(() => {
                console.log('Server.ts : Connected to mongoDb');
                mongoose.connection.on('error', (error: any)=> {
                    console.error(error);
                });
                return;
            })
            .catch(() => {
                if(attempt == 5){
                    return;
                }
                attempt++;
                setTimeout(this.try_connection, 15000);
            })
    }
}
