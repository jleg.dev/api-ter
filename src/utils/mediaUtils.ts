const fs = require('fs');

module.exports.removeImage = function removeImage(path_img: String){
    return new Promise((resolve, reject) => {
        fs.access(path_img,fs.F_OK, error => {
            if(error){
                return reject(error);
            }
            else {
                fs.unlink(path_img, error => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve();
                    }
                });
            }
        })
    });
};

