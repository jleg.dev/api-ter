
export let jwtConstante = require('../utils/constanteApp').JWT_CONSTANT;
var jwt = require('jsonwebtoken');

module.exports = {
    generate_token_limited(user_id){
        return new Promise((resolve, reject) => {

            jwt.sign({ user_id: user_id, id_base:this.generate_random_bytes()}, jwtConstante.PRIVATE_KEY, { expiresIn:"7 days" },function(error,new_token){
                if(error){
                    return reject(error);
                }
                else{
                    return resolve(new_token);
                }
            });
        })
    },
    generate_token_full(user_id, library_id){
        return new Promise((resolve, reject) => {
            jwt.sign({ user_id: user_id, library_id: library_id, id_base:this.generate_random_bytes()}, jwtConstante.PRIVATE_KEY, { expiresIn:"7 days" },function(error,new_token){
                if(error){
                    return reject(error);
                }
                else{
                    return resolve(new_token);
                }
            });
        })
    },
    generate_token_library(library_id){
        return new Promise((resolve, reject) => {
            jwt.sign({library_id: library_id, id_base:this.generate_random_bytes()}, jwtConstante.PRIVATE_KEY, { expiresIn:"7 days" },function(error,new_token){
                if(error){
                    return reject(error);
                }
                else{
                    return resolve(new_token);
                }
            });
        })
    },
    generate_random_bytes(){
        require('crypto').randomBytes(48, function(err, buffer) {
            return buffer.toString('hex');
        });
    }
}
