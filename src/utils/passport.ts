import {mongoose} from "../server";

const passport = require('passport'),
    jwtSecret = require("../utils/constanteApp").JWT_CONSTANT,
    JWTstrategy = require('passport-jwt').Strategy,
    ExtractJWT = require('passport-jwt').ExtractJwt,
    userUtils = require('../utils/userUtils');

const opts = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken('token'),
    secretOrKey: jwtSecret.PRIVATE_KEY,
};

passport.use(
    'jwt_limited',
    new JWTstrategy(opts, (jwt_payload, done) => {
        try {
            mongoose.model('User').findOne({'_id':jwt_payload.user_id})
                .then(user => {
                    if (user) {
                        done(null, jwt_payload);
                    } else {
                        done(null, false);
                    }
            });
        } catch (err) {
            done(err);
        }
    }),
);

passport.use(
    'jwt_full',
    new JWTstrategy(opts, (jwt_payload, done) => {
        try {
            mongoose.model('User').findOne({'_id':jwt_payload.user_id})
                .then(user => {
                    if (user) {
                        mongoose.model('Library').findOne({'_id':jwt_payload.library_id})
                            .then(library => {
                                if(library) {
                                    done(null, jwt_payload);
                                }
                                else{
                                    done(null, false);
                                }
                            })
                            .catch(() => {
                                done(null, false);
                            })
                    }
                    else {
                        done(null, false);
                    }
                });
        } catch (err) {
            done(err);
        }
    }),
);
