export let userConstante = require('../utils/constanteApp').USER_CONSTANT;


module.exports ={
    valid_email(email:string){
        return new Promise((resolve, reject) => {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                return resolve(email)
            }
            return reject('error no valid')
        });
    },
    valid_password(password:string){
        return password.length>=userConstante.PASSWORD_SIZE_MIN;
    },
    userConstante
}
