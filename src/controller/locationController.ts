import { Request, Response } from 'express';
let LocationModel = require('../models/locationModel').LocationModel;
let ExemplaryModel = require('../models/exemplaryModel').ExemplaryModel;
export class LocationController {


    /**
     * @api {get} location Request all
     * @apiName list
     * @apiGroup Location
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of locations retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter locations on their label
     *
     * @apiSuccess {String} total_returned number of locations returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg  locations successfully retrieved
     * @apiSuccess {Array} locations  list of locations
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "total_returned": 1,
     *           "total_page": 1,
     *           "msg": "location successfully retrieved",
     *           "locations": [
     *               {
     *                   "_id": "5ed1501d897732197421a6e6",
     *                   "quantity_max": -1,
     *                   "total_exemplaries": 1,
     *                   "total_items": 1,
     *                   "modified": "2020-05-29T18:07:41.157Z",
     *                   "created": "2020-05-29T18:07:41.157Z",
     *                   "label": "etage 4",
     *                   "location_id": "5ed14fc3897732197421a6e3"
     *               }
     *           ]
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public list(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let keyword = req.query.keyword ?? null;
        //@ts-ignore
        let token = req.user;
        LocationModel.quantityDocuments(null,keyword,token.library_id)
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                LocationModel.retrieveLocations(limit,skip,null,keyword,token.library_id)
                    .then((locations: any) => {
                        res.json({
                            total_returned: locations.length,
                            total_page: totalPage,
                            msg: 'location successfully retrieved',
                            locations: locations
                        });
                        return next();
                    })
                    .catch((error: any) => {
                        res.json({
                            msg: 'error occured during the research',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.json({
                    msg: 'error occured during document count',
                    error: error
                });
                return next([error]);
            })
    }

    /**
     * @api {post} location Create
     * @apiName insert
     * @apiGroup Location
     * @apiPermission Token-Limited
     *
     * @apiParam {String} name name of location
     * @apiParam {String} address address of location
     *
     * @apiSuccess {String} msg successful insertion
     * @apiSuccess {String} id_location id of location created
     * @apiSuccess {Object} location_created location created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successful insertion",
     *           "id_location": "5ed584b2d37fc81c5484e9b4",
     *           "location_created": {
     *               "quantity_max": -1,
     *               "total_exemplaries": 0,
     *               "total_items": 0,
     *               "modified": "2020-06-01T22:44:01.474Z",
     *               "created": "2020-06-01T22:44:01.474Z",
     *               "_id": "5ed584b2d37fc81c5484e9b4",
     *               "label": "etage 4",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during location creation"
     *          "error": "error"
     *     }
     */
    public insert(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let data_location = req.body;
        data_location.library_id = token.library_id;
        let newLocation = new LocationModel(data_location);
        newLocation.save()
            .then(() => {
                res.status(201);
                res.json({
                    msg: 'successful insertion',
                    id_location: newLocation._id,
                    location_created: newLocation.toJSON()
                });
                return next();
            })
            .catch((err: string) => {
                    console.error('LocationController.ts : Creation NewLocation error : '+err);
                    res.status(500);
                    res.json({
                        msg: 'error occured during location creation',
                        error: err
                    });
                    return next([err]);
            });
    }

    /**
     * @api {get} location/:id Request one
     * @apiName show
     * @apiGroup Location
     * @apiPermission Token-Full
     *
     * @apiSuccess {String} msg  locations successfully retrieved
     * @apiSuccess {Object} location location found
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "location found",
     *           "location": {
     *               "_id": "5ed1501d897732197421a6e6",
     *               "quantity_max": -1,
     *               "total_exemplaries": 1,
     *               "total_items": 1,
     *               "modified": "2020-05-29T18:07:41.157Z",
     *               "created": "2020-05-29T18:07:41.157Z",
     *               "label": "etage 4",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) LocationNotFound The <code>id</code> of the Location was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 LocationNotFound
     *     {
     *          "msg": "location not found"
     *          "location_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during location researched"
     *          "error": "error"
     *     }
     */
    public show(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let location_id = req.params.location_id;
        LocationModel.findOne({'_id':location_id,library_id:token.library_id})
            .lean(true)
            .then((location: any) => {
                if(!location) {
                    res.status(404) ;
                    res.json({
                        msg: 'location not found',
                        id_location: location_id
                    });
                    return next();
                }
                res.json({
                    msg: 'location found',
                    location: location
                });
                return next();
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during location researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {put} location/:id Update
     * @apiName update
     * @apiGroup Location
     * @apiPermission Token-Full
     *
     * @apiParam {String} name name of location
     * @apiParam {String} address address of location
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {Objet} location location object upgraded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull upgrad",
     *           "location": {
     *               "quantity_max": -1,
     *               "total_exemplaries": 0,
     *               "total_items": 0,
     *               "modified": "2020-06-01T22:44:01.474Z",
     *               "created": "2020-06-01T22:44:01.474Z",
     *               "_id": "5ed584b2d37fc81c5484e9b4",
     *               "label": "etage 4",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) LocationNotFound The <code>id</code> of the Location was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 LocationNotFound
     *     {
     *          "msg": "location not found"
     *          "location_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during location creation"
     *          "error": "error"
     *     }
     */
    public update(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let location_id = req.params.location_id;
        LocationModel.findOne({'_id':location_id,library_id:token.library_id})
            .then((location: any) => {
                if(!location) {
                    res.status(404);
                    res.json({
                        msg: 'location not found',
                        id_location: location_id
                    });
                    return next();
                }
                location.dynamicUpdate(req.body)
                    .then((location: any) => {
                        res.json({
                            msg: 'successfull upgrad',
                            locations: location,
                        });
                        return next();
                    })
                    .catch((error: any)=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during update',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during location researched',
                    error: error
                });
                return next([error]);
            });
    }


    /**
     * @api {get} location/:id/exemplary Retrieve exemplaries
     * @apiName listExemplaries
     * @apiGroup Location
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of locations retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter locations on their label
     *
     * @apiSuccess {String} msg exemplaries successfully retrieved
     * @apiSuccess {Array} exemplaries  list of exemplaries
     * @apiSuccess {String} total_exemplaries total exemplaries of current location
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} total_returned number of locations returned
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "exemplaries successfully retrieved",
     *           "exemplaries": [
     *               {
     *                   "label": null,
     *                   "modified": "2020-06-01T22:49:23.933Z",
     *                   "created": "2020-05-29T18:20:17.575Z",
     *                   "_id": "5ed152a623ade50988ff9f9d",
     *                   "location": {
     *                       "quantity_max": -1,
     *                       "total_exemplaries": 2,
     *                       "total_items": 1,
     *                       "modified": "2020-05-29T18:07:41.157Z",
     *                       "created": "2020-05-29T18:07:41.157Z",
     *                       "_id": "5ed1501d897732197421a6e6",
     *                       "label": "etage 4",
     *                       "library_id": "5ed14fc3897732197421a6e3"
     *                   },
     *                   "item": {
     *                       "author": "Author test update",
     *                       "editor": "Editor test update",
     *                       "type": "Type test update",
     *                       "date_of_publish": "plzoplzo",
     *                       "total_exemplaries": 2,
     *                       "path_img": "/test",
     *                       "_id": "5ed14ff9897732197421a6e5",
     *                       "category": null,
     *                       "label": "Label test update ",
     *                       "library_id": "5ed14fc3897732197421a6e3",
     *                       "modified": "2020-06-01T22:52:47.582Z",
     *                       "created": "2020-05-29T18:10:01.249Z"
     *                   },
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               {
     *                   "label": null,
     *                   "modified": "2020-06-01T22:39:25.066Z",
     *                   "created": "2020-06-01T22:39:25.066Z",
     *                   "_id": "5ed5839e81d5292798e061af",
     *                   "location": {
     *                       "quantity_max": -1,
     *                       "total_exemplaries": 2,
     *                       "total_items": 1,
     *                       "modified": "2020-05-29T18:07:41.157Z",
     *                       "created": "2020-05-29T18:07:41.157Z",
     *                       "_id": "5ed1501d897732197421a6e6",
     *                       "label": "etage 4",
     *                       "library_id": "5ed14fc3897732197421a6e3"
     *                   },
     *                   "item": {
     *                       "author": "Author test update",
     *                       "editor": "Editor test update",
     *                       "type": "Type test update",
     *                       "date_of_publish": "plzoplzo",
     *                       "total_exemplaries": 2,
     *                       "path_img": "/test",
     *                       "_id": "5ed14ff9897732197421a6e5",
     *                       "category": null,
     *                       "label": "Label test update ",
     *                       "library_id": "5ed14fc3897732197421a6e3",
     *                       "modified": "2020-06-01T22:52:47.582Z",
     *                       "created": "2020-05-29T18:10:01.249Z"
     *                   },
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               }
     *           ],
     *           "total_exemplaries": 2,
     *           "total_page": 1,
     *           "total_returned": 2
     *       }
     *
     *
     * @apiError (404) LocationNotFound The <code>id</code> of the Location was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 LocationNotFound
     *     {
     *          "msg": "location not found"
     *          "location_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during location researched"
     *          "error": "error"
     *     }
     */
    public listExemplaries(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let location_id = req.params.location_id;
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let keyword = req.query.keyword ?? null;


        ExemplaryModel.quantityDocuments({'location':location_id, library_id:token.library_id},keyword)
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                ExemplaryModel.retrieveExemplaries(limit, skip, {'location':location_id},keyword,false,token.library_id)
                    .then(exemplaries => {
                        res.json({
                            msg: 'exemplaries successfully retrieved',
                            exemplaries: exemplaries,
                            total_exemplaries:totalDocument,
                            total_page: totalPage,
                            total_returned: exemplaries.length
                        });
                        return next();
                    })
                    .catch(error => {
                        res.status(500);
                        res.json({
                            msg: 'error occured during location researched',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg: 'error occured during count of exemplaries',
                    error: error
                })
            })
    }


    /**
     * @api {delete} location/:id Delete
     * @apiName delete
     * @apiGroup Location
     * @apiPermission Token-Full
     * @apiDescription A location should be empty to be removed
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {String} total_deleted number of categories deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull suppression",
     *           "total_deleted": 1
     *       }
     *
     *
     * @apiError (404) LocationNotFound The <code>id</code> of the Location was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 LocationNotFound
     *     {
     *          "msg": "location not found"
     *          "location_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during location researched"
     *          "error": "error"
     *     }
     */
    public delete(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let location_id = req.params.location_id;
        ExemplaryModel.quantityDocuments({'location':location_id,library_id:token.library_id},null)
            .then(total => {
                if(total != 0 ){
                    res.status(405);
                    res.json({
                        msg: "Unable to delete this location which is related to existent exemplary. Delete those exemplary before",
                        location_id: location_id,
                        total_exemplary_related: total
                    })
                    return next();
                }
                else{
                    LocationModel.deleteOne({'_id': location_id,library_id:token.library_id})
                        .then((result: { ok: number; deletedCount: number; }) => {
                            if (result.ok == 1 && result.deletedCount == 1) {
                                res.json({
                                    msg: 'successfull suppression',
                                    total_deleted: result.deletedCount
                                });
                                return next();
                            }
                            else{
                                res.status(404);
                                res.json({
                                    msg: 'no location was deleted. may be this item not exist',
                                    location_id: location_id,
                                    total_deleted: result.deletedCount
                                });
                                return next();
                            }
                        })
                        .catch((error: any) => {
                            res.status(500);
                            res.json({
                                msg: 'error occured during location suppression',
                                error: error
                            });
                            return next([error]);
                        });
                }
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg: 'error occured during count of exemplary related',
                    error: error
                })
            })
    }

}
