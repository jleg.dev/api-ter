import { Request, Response } from 'express';
let mediaUtils = require('../utils/mediaUtils');

let mongoose = require('../server').mongoose;



export class MediaController {

    /**
     * @api {post} media/upload_image UploadImage
     * @apiName UploadImage
     * @apiGroup Media
     * @apiPermission Token-Limited
     *
     * @apiParam {Objet} img the file of image
     *
     * @apiSuccess {String} msg image successfully upload
     * @apiSuccess {String} path path of image uploaded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "image successfully upload",
     *           "path_img": "https://api-library-ter.herokuapp.com/static/images/c0eb1a26a781019366cd5d12ffae5d93"
     *       }
     *
     * @apiError (400) ArgumentMissing Mandatory arguments missing
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 ArgumentMissing
     *     {
     *          "msg": "no image was passed to the road. mandatory field : img"
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public uploadImage(req: Request, res: Response, next: any) {
        // @ts-ignore
        console.log(req['file']);
        if (req['file']) {
            let url = (process.env.NODE_ENV==='development'? process.env.URL_DEV : process.env.URL_PROD) + '/' +process.env.PATH_DIRECTORY_STATIC+'/';
            let separator = req['file'].path.indexOf('\\') != -1 ? '\\' : '/';
            let path = url + (req['file'].path
                .split(separator)
                .splice(req['file'].path.indexOf(process.env.DIRECTORY_PUBLIC)+1)
                .join('/'))
            res.status(201);
            res.json({
                msg: 'image successfully upload',
                path_img: path
            })
            return next();
        }
        else{
            res.status(400);
            res.json({
                msg: 'no image was passed to the road. mandatory field : img'
            });
            return next();
        }
    }
}
