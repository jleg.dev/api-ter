import { Request, Response } from 'express';
let UserModel = require('../models/userModel').UserModel;
let LibraryModel = require('../models/libraryModel').LibraryModel;
let mongoose = require('../server').mongoose;
let mediaUtils = require('../utils/mediaUtils');


export class LibraryController {

    /**
     * @api {get} library Request all
     * @apiName list
     * @apiGroup Library
     * @apiPermission Token-Limited
     *
     * @apiParam {Number} limit number of libraries retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter libraries on their label
     *
     * @apiSuccess {String} total_returned number of libraries returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg  libraries successfully retrieved
     * @apiSuccess {Array} libraries  list of libraries
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "library of current user successfully retrieved",
     *           "libraries": [
     *               {
     *                   "modified": "2020-05-29T18:07:40.853Z",
     *                   "created": "2020-05-29T18:07:40.853Z",
     *                   "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTA3NzU3NDcsImV4cCI6MTU5MTM4MDU0N30.glCQAmwCdh8Y6BPEefLLnK_GyXkWYbVyGzepTkXQZk4",
     *                   "path_img": null,
     *                   "_id": "5ed14fc3897732197421a6e3",
     *                   "name": "dzdze",
     *                   "address": "daz",
     *                   "workers": [
     *                       {
     *                           "role": "admin",
     *                           "_id": "5ed14fc3897732197421a6e4",
     *                           "worker": "5ed14f8c897732197421a6e2"
     *                       }
     *                   ]
     *               }
     *           ],
     *           "total_returned": 1,
     *           "total_page": 1
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public list(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let keyword = req.query.keyword ?? null;

        // @ts-ignore
        let token = req.user;
        UserModel.quantityLibraries(token.user_id, keyword)
            .then((totalDocument: any) => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                UserModel.listLibraries(token.user_id, limit, skip, keyword)
                    .then(libraries => {
                        res.json({
                            msg: 'library of current user successfully retrieved',
                            libraries: libraries,
                            total_returned: libraries.length,
                            total_page: totalPage,
                        });
                        return next();
                    })
                    .catch((error: any) => {
                        res.status(500);
                        res.json({
                            msg: 'error occured during the research',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any) => {
                res.status(500);
                res.json({
                    msg: 'error occured during the research',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {post} library Create 
     * @apiName insert
     * @apiGroup Library
     * @apiPermission Token-Limited
     *
     * @apiParam {String} name name of library
     * @apiParam {String} address address of library
     * @apiParam {String} path_img path image of library
     *
     * @apiSuccess {String} msg successful insertion
     * @apiSuccess {String} id_library id of library created
     * @apiSuccess {Object} library_created library created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "library successfully created",
     *           "library": {
     *               "modified": "2020-06-01T22:42:39.950Z",
     *               "created": "2020-06-01T22:42:39.950Z",
     *               "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTEwNTEzNzEsImV4cCI6MTU5MTY1NjE3MX0._KzLpR6SRJUDA-fPQ8KCfJjwMMpAKm82gigI9-O4cmQ",
     *               "path_img": null,
     *               "_id": "5ed5846ba1a59c2c204f8441",
     *               "name": "dzdze",
     *               "address": "daz",
     *               "workers": [
     *                   {
     *                       "role": "admin",
     *                       "_id": "5ed5846ba1a59c2c204f8442",
     *                       "worker": "5ed14f8c897732197421a6e2"
     *                   }
     *               ]
     *           }
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during library creation"
     *          "error": "error"
     *     }
     */
    public insert(req: Request, res: Response, next: any) {
        // @ts-ignore
        let token = req.user;
        if(req.body.name === undefined || req.body.address === undefined){
            res.status(400);
            res.json({
                msg: 'mandatory arguments missing. mandatory arguments : library_name, library_address'
            })
            return next();
        }
        let data_library = req.body;
        data_library.worker = token.user_id;
        data_library.role = 'admin';
        data_library.path_img = req.body.path_img;
        LibraryModel.create(data_library)
            .then(newLibrary=> {
                res.json({
                    msg: 'library successfully created',
                    library: newLibrary
                })
                return next();
            })
            .catch(error => {
                res.status(400);
                res.json({
                    msg: 'error occured during library creation',
                    error: error
                })
                return next(error);
            })
    }

    /**
     * @api {put} library Update 
     * @apiName update
     * @apiGroup Library
     * @apiPermission Token-Full
     *
     * @apiParam {String} [name] name of library
     * @apiParam {String} [address] address of library*
     * @apiParam {String} [path_img] path image of library
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {Objet} library library object upgraded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull upgrad",
     *           "library": {
     *               "modified": "2020-06-01T22:57:53.952Z",
     *               "created": "2020-05-29T18:07:40.853Z",
     *               "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTA3NzU3NDcsImV4cCI6MTU5MTM4MDU0N30.glCQAmwCdh8Y6BPEefLLnK_GyXkWYbVyGzepTkXQZk4",
     *               "path_img": "dazdza",
     *               "_id": "5ed14fc3897732197421a6e3",
     *               "name": "dzdze",
     *               "address": "daz",
     *               "workers": [
     *                   {
     *                       "role": "admin",
     *                       "_id": "5ed14fc3897732197421a6e4",
     *                       "worker": "5ed14f8c897732197421a6e2"
     *                   }
     *               ]
     *           }
     *       }
     *
     *
     * @apiError (404) LibraryNotFound The <code>id</code> of the Library was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 LibraryNotFound
     *     {
     *          "msg": "library not found"
     *          "library_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during library creation"
     *          "error": "error"
     *     }
     */
    public update(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        LibraryModel.findOne({'_id':token.library_id})
            .then(library => {
                if(!library) {
                    res.status(404);
                    res.json({
                        msg: 'library not found',
                        id_library: token.library_id
                    });
                    return next();
                }
                library.dynamicUpdate(req.body)
                    .then(library => {
                        res.json({
                            msg: 'successfull upgrad',
                            library: library,
                        });
                        return next();
                    })
                    .catch(error=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during update',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during library researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {get} /library_users_list Request all users
     * @apiName listUsers
     * @apiGroup Library
     * @apiPermission Token-Full
     * @apiDescription Retrieve all users of current library. Possible only for admin of the library
     *
     * @apiParam {Number} limit number of libraries retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter libraries on their label
     *
     * @apiSuccess {String} total_returned number of libraries returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg users successfully retrieved
     * @apiSuccess {Array} users list of users
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "users successfully retrieved",
     *           "users": [
     *               {
     *                   "role": "admin",
     *                   "_id": "5ed14fc3897732197421a6e4",
     *                   "worker": {
     *                       "_id": "5ed14f8c897732197421a6e2",
     *                       "email": "azerat@a.com",
     *                       "last_name": "eaz",
     *                       "first_name": "eaze"
     *                   }
     *               }
     *           ],
     *           "total_returned": 1,
     *           "total_page": 1
     *       }
     *
     * @apiError (403) NotAutorized Not autorized to effectuate this action
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action
     *     {
     *          "msg": "user not autorized to get the list of users of the current library"
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public listUsers(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page) - 1) * limit : 0;

        // @ts-ignore
        let token = req.user;
        LibraryModel.quantityUsers(token.library_id)
            .then((totalDocument: any) => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                LibraryModel.listUsers(token.library_id, limit, skip)
                    .then(users => {
                        if(users.length > 0 ) {
                            let admin = users.filter(user => {
                                return user.role = 'admin';
                            })
                            if (admin[0].worker._id != token.user_id) {
                                res.status(403);
                                res.json({
                                    msg: 'user not autorized to get the list of users of the current library'
                                })
                                return next();
                            }
                        }
                        res.json({
                            msg: 'users successfully retrieved',
                            users: users,
                            total_returned: users.length,
                            total_page: totalPage,
                        });
                        return next();
                    })
                    .catch((error: any) => {
                        res.status(500);
                        res.json({
                            msg: 'error occured during the research',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any) => {
                res.status(500);
                res.json({
                    msg: 'error occured during the research',
                    error: error
                });
                return next([error]);
            });
    }
}
