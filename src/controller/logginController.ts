import { Request, Response } from 'express';
let UserModel = require('../models/userModel').UserModel;
let userUtils = require('../utils/userUtils');
let tokenUtils = require('../utils/tokenUtils');
let mongoose = require('../server').mongoose;


export class LogginController {

    /**
     * @api {post} register Register
     * @apiName register
     * @apiGroup Login
     * @apiPermission Token-Free
     *
     * @apiParam {String} password password of user
     * @apiParam {String} email email of user
     * @apiParam {String} last_name last name  of user
     * @apiParam {String} first_name first name of user
     *
     * @apiSuccess {String} user successfully created
     * @apiSuccess {Objet} user user created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "user successfully created",
     *           "user": {
     *               "libraries": [],
     *               "modified": "2020-06-01T23:53:24.336Z",
     *               "created": "2020-06-01T23:53:24.336Z",
     *               "token": null,
     *               "_id": "5ed5951a04c3d8459847559a",
     *               "email": "compte@test.com",
     *               "last_name": "Bernard",
     *               "first_name": "DuTest"
     *           }
     *       }

     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public register(req: Request, res: Response, next: any) {
        let newUser = new UserModel(req.body);
        newUser.save()
            .then(()=> {
                res.json({
                    msg: 'user successfully created',
                    user: newUser
                })
                return next();
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg: 'unable to create a user',
                    error: error.message
                })
                return next([error]);
            })
    }

    /**
     * @api {post} login Login
     * @apiName Login
     * @apiGroup Login
     * @apiPermission Token-Free
     * @apiDescription Check credentials. Create and return a token Limited if check is valid.
     *
     * @apiParam {String} password password of user
     * @apiParam {String} email email of user
     * @apiParam {String} last_name last name  of user
     * @apiParam {String} first_name first name of user
     *
     * @apiSuccess {String} msg successfull connection to library
     * @apiSuccess {String} token token full created
     * @apiSuccess {Object} user current_user
     *
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "authentification success",
     *           "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTU2OTk5ZmRkYmQyYTNjZTMwYjBkIiwiaWF0IjoxNTkxMTEyMDMyLCJleHAiOjE1OTE3MTY4MzJ9.TEyfKTzK25gFuZYRbKvsbY0UlTYbYGEDi5HDCNKUbJ4",
     *           "user": {
     *               "modified": "2020-05-29T18:38:07.731Z",
     *               "created": "2020-05-29T18:38:07.731Z",
     *               "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTU2OTk5ZmRkYmQyYTNjZTMwYjBkIiwiaWF0IjoxNTkxMTEyMDMyLCJleHAiOjE1OTE3MTY4MzJ9.TEyfKTzK25gFuZYRbKvsbY0UlTYbYGEDi5HDCNKUbJ4",
     *               "_id": "5ed156999fddbd2a3ce30b0d",
     *               "email": "azerataa@a.com",
     *               "last_name": "eaz",
     *               "first_name": "eaze"
     *           }
     *       }
     *
     * @apiError (400) ArgumentMissing Mandatory arguments missing
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 ArgumentMissing
     *     {
     *          "msg": "mandatory arguments missing. mandatory arguments : email, password"
     *     }
     *
     * @apiError (401) AuthenticationFailed AuthenticationFailed
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 AuthenticationFailed
     *     {
     *          "msg": "authentification failed"
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public login(req: Request, res: Response, next: any) {
        let email = req.body.email;
        let password = req.body.password;
        if(!email || !password){
            res.status(400);
            res.json({
                msg: 'mandatory arguments missing. mandatory arguments : email, password'
            })
            return next();
        }
        else{
            UserModel.findOne({'email':email})
                .then(user => {
                    if (user) {
                        user.comparePassword(password).then(()=>{
                            tokenUtils.generate_token_limited(user._id)
                                .then(token => {
                                    user.token = token;
                                    user.save()
                                        .then(() => {
                                            user.libraries = undefined;
                                            user.password = undefined;
                                            res.json({
                                                msg: 'authentification success',
                                                token: token,
                                                user: user
                                            })
                                            return next();
                                        })
                                        .catch(error => {
                                            res.status(500);
                                            res.json({
                                                msg: 'an error occured'
                                            })
                                            return next([error]);
                                        })

                                })
                                .catch(error => {
                                    console.error(error);
                                    res.json({
                                        msg: 'an error occured'
                                    })
                                    return next(error);
                                })
                        })
                        .catch(error => {
                            res.status(401);
                            res.json({
                                msg: 'authentification failed',
                                error:error
                            })
                            return next();
                        })
                }
                else{
                    res.status(401);
                    res.json({
                        msg: 'authentification failed'
                    })
                    return next();
                }
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg: 'an error occured'
                })
                return next(error);
            })
        }
    }

    /**
     * @api {post} /library_connect/:id LibraryConnect
     * @apiName LibraryConnect
     * @apiGroup Login
     * @apiPermission Token-Limited
     * @apiDescription Connect to a library is user is member of library. Create and return a token Full if ok.
     *
     *
     * @apiSuccess {String} msg successfull connection to library
     * @apiSuccess {String} new_token token full created
     * @apiSuccess {String} role role of current user in this library
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull connection to library",
     *           "new_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTRmOGM4OTc3MzIxOTc0MjFhNmUyIiwibGlicmFyeV9pZCI6IjVlZDE0ZmMzODk3NzMyMTk3NDIxYTZlMyIsImlhdCI6MTU5MTA1NTk3OCwiZXhwIjoxNTkxNjYwNzc4fQ.fbBMlnIQgcq9FFqkqCzr8GPgvZWDRpUm2UeCqKUN_4U",
     *           "role": "admin"
     *       }
     *
     * @apiError (403) NotAutorized Not autorized to effectuate this action
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action
     *     {
     *          "msg": "this user has no right to connect to this library"
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public connectLibrary(req: Request, res: Response, next: any) {
        let library_id = req.params.library_id;
        // @ts-ignore
        let token = req.user;
        mongoose.model('Library').findOne({_id:library_id, 'workers.worker':token.user_id})
            .then(library=>{
                if(library) {
                    tokenUtils.generate_token_full(token.user_id,library._id)
                        .then(new_token => {

                            let current_user = library.workers.find(function(user){
                                return user.worker == token.user_id;
                            })
                            res.json({
                                msg: 'successfull connection to library',
                                new_token: new_token,
                                role: current_user.role
                            })
                            return next();
                        })
                        .catch(error => {
                            res.status(500)
                            res.json({
                                msg: 'an error occured'
                            })
                            return next(error);
                        })
                }
                else{
                    res.status(403);
                    res.json({
                        msg: 'this user has no right to connect to this library'
                    })
                    return next();
                }
            })
    }


    /**
     * @api {post} /library_join LibraryJoin
     * @apiName LibraryJoin
     * @apiGroup Login
     * @apiPermission Token-Limited
     * @apiDescription Join a library with the token of library. Permanent added.
     *
     *
     * @apiParam {String} token_library token of library
     * @apiSuccess {String} msg successfull join the new library
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull join the new library"
     *       }
     *
     * @apiError (400) ArgumentMissing Mandatory arguments missing
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 ArgumentMissing
     *     {
     *          "msg": "mandatory arguments missing. mandatory arguments : email, password"
     *     }
     *
     *
     * @apiError (403) NotAutorized Not autorized to effectuate this action
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action
     *     {
     *          "msg": "this user has no right to connect to this library"
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public joinLibrary(req: Request, res: Response, next: any) {
        // @ts-ignore
        let token = req.user;
        let token_library = req.body.token_library;
        if(token_library === undefined){
            res.status(400);
            res.json({
                msg: 'mandatory arguments missing : token_library'
            })
            return next();
        }
        mongoose.model('Library').findOne({'token':token_library})
            .then(library=>{
                if(library) {
                    tokenUtils.generate_token_library({_id:library._id})
                        .then(new_token => {
                            let new_worker = {'worker':token.user_id,'role':'worker'};
                            library.workers.push(new_worker);
                            library.token = new_token;
                            library.save()
                                .then(()=> {
                                    res.json({
                                        msg: 'successfull join the new library'
                                    })
                                    return next();
                                })
                                .catch(error => {
                                    res.status(500);
                                    res.json({
                                        msg: 'an error occured'
                                    })
                                })
                        })
                        .catch(error => {
                            res.json({
                                msg: 'an error occured'
                            })
                            return next(error);
                        })
                }
                else{
                    res.status(403);
                    res.json({
                        msg: 'no library found'
                    })
                    return next();
                }
            })
    }


}
