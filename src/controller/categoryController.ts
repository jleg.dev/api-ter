import { Request, Response } from 'express';
let CategoryModel = require('../models/categoryModel').CategoryModel;
let ItemModel = require('../models/itemModel').ItemModel;



export class CategoryController {

    /**
     * @api {get} category Request all
     * @apiName list
     * @apiGroup Category
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of categories retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter categories on their label
     *
     * @apiSuccess {String} total_returned number of categories returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg  categories successfully retrieved
     * @apiSuccess {Array} categories  list of categories
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *      "total_returned": 1,
     *      "total_page": 1,
     *      "msg": "categories successfully retrieved",
     *      "categories": [
     *           {
     *               "_id": "5ed151de16ab5e26109cd068",
     *               "modified": "2020-05-29T18:17:41.744Z",
     *               "created": "2020-05-29T18:17:41.744Z",
     *               "label": "other",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *        ]
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public list(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let keyword = req.query.keyword ?? null;
        // @ts-ignore
        let token = req.user;
        CategoryModel.quantityDocuments(keyword, token.library_id)
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                CategoryModel.retrieveCategories(limit, skip, token.library_id, keyword)
                    .then((categories: any) => {
                        res.json({
                            total_returned: categories.length,
                            total_page: totalPage,
                            msg: 'categories successfully retrieved',
                            categories: categories
                        });
                        return next();
                    })
                    .catch((error: any)=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during the research',
                            error: error
                        })
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during document count',
                    error: error
                });
                return next([error]);
            })
    }

    /**
     * @api {post} category Create
     * @apiName insert
     * @apiGroup Category
     * @apiPermission Token-Full
     *
     * @apiParam {String} label label of category
     *
     * @apiSuccess {String} msg successful insertion
     * @apiSuccess {String} id_category id of category created
     * @apiSuccess {Object} category_created category created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successful insertion",
     *           "id_category": "5ed581089e689f18c0b5695f",
     *           "category_created": {
     *               "modified": "2020-06-01T22:28:14.836Z",
     *               "created": "2020-06-01T22:28:14.836Z",
     *               "_id": "5ed581089e689f18c0b5695f",
     *               "label": "other",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during category creation"
     *          "error": "error"
     *     }
     */
    public insert(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let data_category = req.body;
        data_category.library_id = token.library_id;
        let newCategory = new CategoryModel(data_category);
        newCategory.save()
            .then( () => {
                res.status(201);
                res.json({
                    msg: 'successful insertion',
                    id_category: newCategory._id,
                    category_created: newCategory.toJSON()
                });
                return next();
            })
            .catch((err: string) => {
                console.error('CategoryController.ts : Creation NewCategory error : '+err);
                res.status(500);
                res.json({
                    msg: 'error occured during category creation',
                    error: err
                });
                return next([err]);
            })
    }

    /**
     * @api {get} category/:id Request one
     * @apiName show
     * @apiGroup Category
     * @apiPermission Token-Full
     *
     * @apiSuccess {String} msg  locations successfully retrieved
     * @apiSuccess {Object} category category found
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "category found",
     *           "category": {
     *               "_id": "5ed151de16ab5e26109cd068",
     *               "modified": "2020-05-29T18:17:41.744Z",
     *               "created": "2020-05-29T18:17:41.744Z",
     *               "label": "other",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) CategoryNotFound The <code>id</code> of the Category was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 CategoryNotFound
     *     {
     *          "msg": "category not found"
     *          "category_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during category researched"
     *          "error": "error"
     *     }
     */
    public show(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let category_id = req.params.category_id;
        CategoryModel.findOne({_id:category_id, library_id:token.library_id}).lean(true)
            .then((category: any) => {
                if(!category) {
                    res.status(404) ;
                    res.json({
                        msg: 'category not found',
                        id_category: category_id
                    });
                    return next();
                }
                res.json({
                    msg: 'category found',
                    category: category,
                });
                return next();
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during category researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {put} category/:id Update
     * @apiName update
     * @apiGroup Category
     * @apiPermission Token-Full
     *
     * @apiParam {String} [label] label of category
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {Objet} category category object upgraded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull upgrad",
     *           "category": {
     *               "modified": "2020-06-01T22:48:03.770Z",
     *               "created": "2020-05-29T18:17:41.744Z",
     *               "_id": "5ed151de16ab5e26109cd068",
     *               "label": "fraise",
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) CategoryNotFound The <code>id</code> of the Category was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 CategoryNotFound
     *     {
     *          "msg": "category not found"
     *          "category_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": error occured during category creation
     *          "error": "error"
     *     }
     */
    public update(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let category_id = req.params.category_id;
        CategoryModel.findOne({_id:category_id,library_id:token.library_id})
            .then((category: any) => {
                if(!category) {
                    res.status(404);
                    res.json({
                        msg: 'category not found',
                        id_category: category_id
                    });
                    return next();
                }
                category.dynamicUpdate(req.body)
                    .then((category: any) => {
                        res.json({
                            msg: 'successfull upgrad',
                            category: category,
                        });
                        return next();
                    })
                    .catch((error: any)=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during update',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during category researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {delete} category/:id Delete
     * @apiName delete
     * @apiGroup Category
     * @apiPermission Token-Full
     *
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {String} total_deleted number of categories deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull suppression",
     *           "total_deleted": 1
     *       }
     *
     *
     * @apiError (404) CategoryNotFound The <code>id</code> of the Category was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 CategoryNotFound
     *     {
     *          "msg": "category not found"
     *          "category_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during category researched"
     *          "error": "error"
     *     }
     */
    public delete(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let category_id = req.params.category_id;
        CategoryModel.deleteOne({_id:category_id,library_id:token.library_id})
            .then((result: { ok: number; deletedCount: number; }) => {
                if(result.ok==1 && result.deletedCount == 1) {
                    // We deleted all categories reference in items collection
                    ItemModel.find({categories: {'$all': [category_id]}, library_id: token.library_ids})
                        .then((items: any[]) => {
                            items.forEach(
                                item => {
                                    item.categories.splice(item.categories.indexOf(category_id, 0 )+1,1);
                                    item.modified = Date.now();
                                    item.save();
                                }
                            )
                            res.json({
                                msg: 'successfull suppression',
                                total_deleted: result.deletedCount,
                                item_updated: items.length
                            });
                            return next();
                        })
                        .catch((error: any) => {
                            res.status(500);
                            res.json({
                                msg: 'error occured during items matching category researched',
                                error: error
                            });
                            return next([error]);
                        })
                }
                else{
                    res.status(404);
                    res.json({
                        msg: 'not successfull suppression. category might not exist.',
                        total_deleted: result.deletedCount
                    });
                    return next();
                }
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during category researched',
                    error: error
                });
                return next([error]);
            });
    }
}
