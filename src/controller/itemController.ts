import { Request, Response } from 'express';
let ItemModel = require('../models/itemModel').ItemModel;
let ExemplaryModel = require('../models/exemplaryModel').ExemplaryModel;
let mediaUtils = require('../utils/mediaUtils');

let mongoose = require('../server').mongoose;



export class ItemController {

    /**
     * @api {get} item Request all
     * @apiName list
     * @apiGroup Item
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of items retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter items on their label
     *
     * @apiSuccess {String} total_returned number of items returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg  items successfully retrieved
     * @apiSuccess {Array} items  list of items
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *      "total_returned": 1,
     *      "total_page": 1,
     *      "msg": "items successfully retrieved",
     *      "items": [
     *      {
     *          "total_returned": 1,
     *          "total_page": 1,
     *          "msg": "items successfully retrieved",
     *          "items": [
     *           {
     *              "_id": "5ed14ff9897732197421a6e5",
     *              "author": "patrick",
     *              "editor": "maplplzodzdz",
     *              "type": "book",
     *              "date_of_publish": "plzoplzo",
     *              "total_exemplaries": 1,
     *              "path_img": "/test",
     *              "category": null,
     *              "label": " dzadaz",
     *              "library_id": "5ed14fc3897732197421a6e3",
     *              "modified": "2020-05-29T18:10:01.249Z",
     *              "created": "2020-05-29T18:10:01.249Z"
     *          }
     *          ]
     *      }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public list(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let attr = req.query.attr ?? null;
        let value = req.query.value ?? null;
        let keyword = req.query.keyword ?? null;
        //@ts-ignore
        let token = req.user;
        ItemModel.quantityDocuments(keyword, attr, value)
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                ItemModel.retrieveItem(limit, skip, keyword, attr, value, token.library_id)
                    .then((items: any) => {
                        res.json({
                            total_returned: items.length,
                            total_page: totalPage,
                            msg: 'items successfully retrieved',
                            items: items
                        });
                        return next();
                    })
                    .catch((error: any)=> {
                        res.json({
                            msg: 'error occured during the research',
                            error: error
                        })
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.json({
                    msg: 'error occured during document count',
                    error: error
                });
                return next([error]);
            })
    }

    /**
     * @api {post} item Create
     * @apiName insert
     * @apiGroup Item
     * @apiPermission Token-Full
     *
     * @apiParam {String} label label of item
     * @apiParam {String} author author of item
     * @apiParam {String} editor editor of item
     * @apiParam {String} type type of item
     * @apiParam {String} date_of_publish date of publication of  item
     * @apiParam {String} isbn isbn of item
     * @apiParam {String} category category_id of item
     * @apiParam {String} path_img path of image of item
     *
     * @apiSuccess {String} msg successful insertion
     * @apiSuccess {String} id_item id of item created
     * @apiSuccess {Object} item_created item created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successful insertion",
     *           "id_item": "5ed584043e24cd285cce0681",
     *           "item_created": {
     *               "author": "Patrick Descamps",
     *               "editor": "Flammarion",
     *               "type": "Book",
     *               "date_of_publish": "2020-04-05",
     *               "total_exemplaries": 0,
     *               "path_img": "http://google.image.fr/fourshadow.png",
     *               "_id": "5ed584043e24cd285cce0681",
     *               "category": null,
     *               "label": "Four shadows",
     *               "library_id": "5ed14fc3897732197421a6e3",
     *               "modified": "2020-06-01T22:41:08.841Z",
     *               "created": "2020-06-01T22:41:08.841Z"
     *           }
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during item creation"
     *          "error": "error"
     *     }
     */
    public insert(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let data_item = req.body;
        data_item.library_id = token.library_id;

        let newItem = new ItemModel(data_item);
        newItem.save()
            .then(() => {
                res.status(201);
                res.json({
                    msg: 'successful insertion',
                    id_item: newItem._id,
                    item_created: newItem.toJSON()
                });
                return next();
            })
            .catch((err: string) => {
                if(err){
                    console.error('ItemController.ts : Creation NewItem error : '+err);
                    res.status(500);
                    res.json({
                        msg: 'error occured during item creation',
                        error: err
                    });
                    return next([err]);
            }
        });


    }

    /**
     * @api {get} item/:id Request one
     * @apiName show
     * @apiGroup Item
     * @apiPermission Token-Full
     *
     * @apiSuccess {String} msg  locations successfully retrieved
     * @apiSuccess {Object} item item found
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "item found",
     *           "item": {
     *               "_id": "5ed14ff9897732197421a6e5",
     *               "author": "patrick",
     *               "editor": "maplplzodzdz",
     *               "type": "book",
     *               "date_of_publish": "plzoplzo",
     *               "total_exemplaries": 1,
     *               "path_img": "/test",
     *               "category": null,
     *               "label": " dzadaz",
     *               "library_id": "5ed14fc3897732197421a6e3",
     *               "modified": "2020-05-29T18:10:01.249Z",
     *               "created": "2020-05-29T18:10:01.249Z"
     *           }
     *       }
     *
     *
     * @apiError (404) ItemNotFound The <code>id</code> of the Item was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ItemNotFound
     *     {
     *          "msg": "item not found"
     *          "item_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during item researched"
     *          "error": "error"
     *     }
     */
    public show(req: Request, res: Response, next: any){
        let item_id = req.params.item_id;
        //@ts-ignore
        let token = req.user;
        ItemModel.findOne({'_id':item_id,library_id:token.library_id})
            .lean({ autopopulate: true })
            .then((item: any) => {
                if(!item) {
                    res.status(404) ;
                    res.json({
                        msg: 'item not found',
                        id_item: item_id
                    });
                    return next();
                }

                res.json({
                    msg: 'item found',
                    item: item,
                });
                return next();
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during item researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {put} item/:id Update
     * @apiName update
     * @apiGroup Item
     * @apiPermission Token-Full
     *
     * @apiParam {String} [label] label of item
     * @apiParam {String} [author] author of item
     * @apiParam {String} [editor] editor of item
     * @apiParam {String} [type type] of item
     * @apiParam {String} [date_of_publish] date of publication of  item
     * @apiParam {String} [isbn isbn] of item
     * @apiParam {String} [category] category_id of item
     * @apiParam {String} [path_img] path of image of item
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {Objet} item item object upgraded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull upgrad",
     *           "item": {
     *               "author": "Author test update",
     *               "editor": "Editor test update",
     *               "type": "Type test update",
     *               "date_of_publish": "plzoplzo",
     *               "total_exemplaries": 2,
     *               "path_img": "/test",
     *               "_id": "5ed14ff9897732197421a6e5",
     *               "category": null,
     *               "label": "Label test update ",
     *               "library_id": "5ed14fc3897732197421a6e3",
     *               "modified": "2020-06-01T22:52:47.582Z",
     *               "created": "2020-05-29T18:10:01.249Z"
     *           }
     *       }
     *
     *
     * @apiError (404) ItemNotFound The <code>id</code> of the Item was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ItemNotFound
     *     {
     *          "msg": "item not found"
     *          "item_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during item creation"
     *          "error": "error"
     *     }
     */
    public update(req: Request, res: Response, next: any){
        let item_id = req.params.item_id;
        //@ts-ignore
        let token = req.user;
        ItemModel.findOne({'_id':item_id, library_id:token.library_id})
            .then(item => {
                if(!item) {
                    res.status(404);
                    res.json({
                        msg: 'item not found',
                        id_item: item_id
                    });
                    return next();
                }
                if(req.body.path_img && item.path_img && req.body.path_img != item.path_img){
                    if(item.path_img.indexOf(process.env.NODE_ENV === 'development' ? process.env.URL_DEV : process.env.URL_PROD) != -1) {
                        let path = process.cwd()+item.path_img.replace(process.env.PATH_DIRECTORY_STATIC, process.env.DIRECTORY_PUBLIC).replace((process.env.NODE_ENV === 'development' ? process.env.URL_DEV : process.env.URL_PROD)+':'+process.env.PORT,'');
                        mediaUtils.removeImage(path).catch(() => {});
                    }
                }
                item.dynamicUpdate(req.body)
                    .then(item => {
                        res.json({
                            msg: 'successfull upgrad',
                            item: item,
                        });
                        return next();
                    })
                    .catch(error=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during update',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during item researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {get} item/:id/exemplary Retrieve exemplaries of item
     * @apiName listExemplaries
     * @apiGroup Item
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of items retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter items on their label
     *
     * @apiSuccess {String} msg  exemplaries successfully retrieved
     * @apiSuccess {Array} exemplaries  list of exemplaries
     * @apiSuccess {String} total_exemplaries total exemplaries of current item
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} total_returned number of items returned
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "exemplaries successfully retrieved",
     *           "exemplaries": [
     *               {
     *                   "label": null,
     *                   "modified": "2020-06-01T22:49:23.933Z",
     *                   "created": "2020-05-29T18:20:17.575Z",
     *                   "_id": "5ed152a623ade50988ff9f9d",
     *                   "item": {
     *                       "quantity_max": -1,
     *                       "total_exemplaries": 2,
     *                       "total_items": 1,
     *                       "modified": "2020-05-29T18:07:41.157Z",
     *                       "created": "2020-05-29T18:07:41.157Z",
     *                       "_id": "5ed1501d897732197421a6e6",
     *                       "label": "etage 4",
     *                       "library_id": "5ed14fc3897732197421a6e3"
     *                   },
     *                   "item": {
     *                       "author": "Author test update",
     *                       "editor": "Editor test update",
     *                       "type": "Type test update",
     *                       "date_of_publish": "plzoplzo",
     *                       "total_exemplaries": 2,
     *                       "path_img": "/test",
     *                       "_id": "5ed14ff9897732197421a6e5",
     *                       "category": null,
     *                       "label": "Label test update ",
     *                       "library_id": "5ed14fc3897732197421a6e3",
     *                       "modified": "2020-06-01T22:52:47.582Z",
     *                       "created": "2020-05-29T18:10:01.249Z"
     *                   },
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               {
     *                   "label": null,
     *                   "modified": "2020-06-01T22:39:25.066Z",
     *                   "created": "2020-06-01T22:39:25.066Z",
     *                   "_id": "5ed5839e81d5292798e061af",
     *                   "item": {
     *                       "quantity_max": -1,
     *                       "total_exemplaries": 2,
     *                       "total_items": 1,
     *                       "modified": "2020-05-29T18:07:41.157Z",
     *                       "created": "2020-05-29T18:07:41.157Z",
     *                       "_id": "5ed1501d897732197421a6e6",
     *                       "label": "etage 4",
     *                       "library_id": "5ed14fc3897732197421a6e3"
     *                   },
     *                   "item": {
     *                       "author": "Author test update",
     *                       "editor": "Editor test update",
     *                       "type": "Type test update",
     *                       "date_of_publish": "plzoplzo",
     *                       "total_exemplaries": 2,
     *                       "path_img": "/test",
     *                       "_id": "5ed14ff9897732197421a6e5",
     *                       "category": null,
     *                       "label": "Label test update ",
     *                       "library_id": "5ed14fc3897732197421a6e3",
     *                       "modified": "2020-06-01T22:52:47.582Z",
     *                       "created": "2020-05-29T18:10:01.249Z"
     *                   },
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               }
     *           ],
     *           "total_exemplaries": 2,
     *           "total_page": 1,
     *           "total_returned": 2
     *       }
     *
     *
     * @apiError (404) ItemNotFound The <code>id</code> of the Item was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ItemNotFound
     *     {
     *          "msg": "item not found"
     *          "item_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during item researched"
     *          "error": "error"
     *     }
     */
    public listExemplaries(req: Request, res: Response, next: any){
        let item_id = req.params.item_id
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        let keyword = req.query.keyword ?? null;
        //@ts-ignore
        let token = req.user;
        ExemplaryModel.quantityDocuments({item:item_id, library_id:token.library_id},keyword)
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1);
                ExemplaryModel.retrieveExemplaries(limit, skip, {item:item_id}, keyword, true,token.library_id)
                    .then(exemplaries => {
                        res.json({
                            msg: 'exemplaries successfully retrieved',
                            exemplaries: exemplaries,
                            total_page: totalPage,
                            total_returned: exemplaries.length,
                            total_exemplaries:totalDocument
                        });
                        return next();
                    })
                    .catch(error => {
                        res.status(500);
                        res.json({
                            msg: 'error occured during exemplaries researched',
                            error: error
                        });
                        return next(error);
                    })
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg:'error occured during the count of documents',
                    error:error
                })
            })
    }

    /**
     * @api {delete} item/:id Delete
     * @apiName delete
     * @apiGroup Item
     * @apiPermission Token-Full
     * @apiDescription An item should not refer to existent exemplaries to be removed
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {String} total_deleted number of categories deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull suppression",
     *           "total_deleted": 1
     *       }
     *
     *
     * @apiError (404) ItemNotFound The <code>id</code> of the Item was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ItemNotFound
     *     {
     *          "msg": "item not found"
     *          "item_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during item researched"
     *          "error": "error"
     *     }
     */
    public delete(req: Request, res: Response, next: any){
        let item_id = req.params.item_id;
        //@ts-ignore
        let token = req.user;
        ExemplaryModel.countDocuments({item:item_id, library_id:token.library_id})
            .then(total => {
                if(total > 0 ){
                    res.status(405);
                    res.json({
                        msg: "Unable to delete this item which is related to existent exemplary. Delete those exemplary before",
                        item_id: item_id,
                        total_exemplary_related: total
                    })
                    return next();
                }
                else{
                    ItemModel.deleteOne({'_id': item_id, library_id:token.library_id})
                        .then((result: { ok: number; deletedCount: number; }) => {
                            if (result.ok == 1 && result.deletedCount == 1) {
                                res.json({
                                    msg: 'successfull suppression',
                                    total_deleted: result.deletedCount
                                });
                                return next();
                            }
                            else{
                                res.status(404);
                                res.json({
                                    msg: 'no item was deleted. may be this item not exist',
                                    item_id: item_id,
                                    total_deleted: result.deletedCount
                                });
                                return next();
                            }
                        })
                        .catch((error: any) => {
                            res.status(500);
                            res.json({
                                msg: 'error occured during item suppression',
                                error: error
                            });
                            return next([error]);
                        });
                }
            })
            .catch(error => {
                res.status(500);
                res.json({
                    msg: 'error occured during count of exemplary related',
                    error: error
                })
            })
    }
}
