import { Request, Response } from 'express';
let ExemplaryModel = require('../models/exemplaryModel').ExemplaryModel;
let LocationModel = require('../models/locationModel').LocationModel;
let ItemModel = require('../models/itemModel').ItemModel;

export class ExemplaryController {

    /**
     * @api {get} exemplary Request all
     * @apiName list
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     *
     * @apiParam {Number} limit number of exemplaries retrieved
     * @apiParam {Number} page number of page paginated
     * @apiParam {String} keyword characted used to filter exemplaries on their label
     *
     * @apiSuccess {String} total_returned number of exemplaries returned
     * @apiSuccess {String} total_page  total number of page
     * @apiSuccess {String} msg  exemplaries successfully retrieved
     * @apiSuccess {Array} exemplaries  list of exemplaries
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *      "total_returned": 1,
     *      "total_page": 1,
     *      "msg": "exemplaries successfully retrieved"
     *      "exemplaries": [
     *       {
     *              "_id": "5ed152a623ade50988ff9f9d",
     *              "label": null,
     *              "modified": "2020-05-29T18:20:17.575Z",
     *              "created": "2020-05-29T18:20:17.575Z",
     *              "location": {
     *                  "_id": "5ed1501d897732197421a6e6",
     *                  "quantity_max": -1,
     *                  "total_exemplaries": 1,
     *                  "total_items": 1,
     *                  "modified": "2020-05-29T18:07:41.157Z",
     *                  "created": "2020-05-29T18:07:41.157Z",
     *                  "label": "etage 4",
     *                  "library_id": "5ed14fc3897732197421a6e3"
     *              },
     *              "item": {
     *                  "_id": "5ed14ff9897732197421a6e5",
     *                  "author": "patrick",
     *                  "editor": "maplplzodzdz",
     *                  "type": "book",
     *                  "date_of_publish": "plzoplzo",
     *                  "total_exemplaries": 1,
     *                  "path_img": "/test",
     *                  "category": null,
     *                  "label": " dzadaz",
     *                  "library_id": "5ed14fc3897732197421a6e3",
     *                  "modified": "2020-05-29T18:10:01.249Z",
     *                  "created": "2020-05-29T18:10:01.249Z"
     *              },
     *              "library_id": "5ed14fc3897732197421a6e3"
     *        }]
     *     }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during document count / research"
     *          "error": "error"
     *     }
     */
    public list(req: Request, res: Response, next: any) {
        let limit = parseInt(<string>req.query.limit ?? 15);
        let skip = req.query.page ? (parseInt(<string>req.query.page )-1)  * limit : 0 ;
        //@ts-ignore
        let token = req.user;
        ExemplaryModel.countDocuments({library_id:token.library_id})
            .then(totalDocument => {
                let totalPage = Math.ceil(totalDocument > limit ? totalDocument / limit : 1) ;
                ExemplaryModel.find({library_id:token.library_id})
                .limit(limit)
                .skip(skip)
                .lean({ autopopulate: true })
                .then((exemplaries: any) => {
                    res.json({
                        total_returned: exemplaries.length,
                        total_page: totalPage,
                        msg: 'exemplary successfully retrieved',
                        exemplaries: exemplaries
                    });
                    return next();
                })
                .catch((error: any)=> {
                    res.json({
                        msg: 'error occured during the research',
                        error: error
                    });
                    return next([error]);
                });
            })
            .catch((error: any)=> {
                res.json({
                    msg: 'error occured during document count',
                    error: error
                });
                return next([error]);
            })
    }


    /**
     * @api {post} exemplary Create
     * @apiName insert
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     *
     * @apiParam {String} label label of exemplary
     * @apiParam {String} location location_id of exemplary
     * @apiParam {String} item label_id of exemplary
     *
     * @apiSuccess {String} msg successful insertion
     * @apiSuccess {String} id_exemplary id of exemplary created
     * @apiSuccess {Object} exemplary_created exemplary created
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successful insertion",
     *           "id_exemplary": "5ed5839e81d5292798e061af",
     *           "exemplary_created": {
     *               "label": null,
     *               "modified": "2020-06-01T22:39:25.066Z",
     *               "created": "2020-06-01T22:39:25.066Z",
     *               "_id": "5ed5839e81d5292798e061af",
     *               "location": {
     *                   "quantity_max": -1,
     *                   "total_exemplaries": 1,
     *                   "total_items": 1,
     *                   "modified": "2020-05-29T18:07:41.157Z",
     *                   "created": "2020-05-29T18:07:41.157Z",
     *                   "_id": "5ed1501d897732197421a6e6",
     *                   "label": "etage 4",
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               "item": {
     *                   "author": "patrick",
     *                   "editor": "maplplzodzdz",
     *                   "type": "book",
     *                   "date_of_publish": "plzoplzo",
     *                   "total_exemplaries": 1,
     *                   "path_img": "/test",
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "category": null,
     *                   "label": " dzadaz",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-05-29T18:10:01.249Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during exemplary creation"
     *          "error": "error"
     *     }
     */
    public insert(req: Request, res: Response, next: any){
        let data_exemplary = req.body;
        //@ts-ignore
        let token = req.user;
        data_exemplary.library_id = token.library_id;
        let newExemplary = new ExemplaryModel(data_exemplary);
        newExemplary.save()
            .then(() => {
                ExemplaryModel.updateQuantity(newExemplary,newExemplary.location);
                res.status(201)
                res.json({
                    msg: 'successful insertion',
                    id_exemplary: newExemplary._id,
                    exemplary_created: newExemplary.toJSON()
                });
            })
            .catch((error: string) => {
                if(error){
                    console.error('ExemplaryController.ts : Creation NewExemplary error : '+error);
                    res.status(500);
                    res.json({
                        msg: 'error occured during exemplary creation',
                        error: error
                    });
                    return next([error]);
                }
            });

    }

    /**
     * @api {get} exemplary/:id Request one
     * @apiName show
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     *
     * @apiSuccess {String} msg  locations successfully retrieved
     * @apiSuccess {Object} exemplary exemplary found
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "exemplary found",
     *           "exemplary": {
     *               "_id": "5ed152a623ade50988ff9f9d",
     *               "label": null,
     *               "modified": "2020-05-29T18:20:17.575Z",
     *               "created": "2020-05-29T18:20:17.575Z",
     *               "location": {
     *                   "_id": "5ed1501d897732197421a6e6",
     *                   "quantity_max": -1,
     *                   "total_exemplaries": 1,
     *                   "total_items": 1,
     *                   "modified": "2020-05-29T18:07:41.157Z",
     *                   "created": "2020-05-29T18:07:41.157Z",
     *                   "label": "etage 4",
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               "item": {
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "author": "patrick",
     *                   "editor": "maplplzodzdz",
     *                   "type": "book",
     *                   "date_of_publish": "plzoplzo",
     *                   "total_exemplaries": 1,
     *                   "path_img": "/test",
     *                   "category": null,
     *                   "label": " dzadaz",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-05-29T18:10:01.249Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public show(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        ExemplaryModel.findOne({'_id':exemplary_id, library_id:token.library_id})
            .lean({ autopopulate: true })
            .then((exemplary: any) => {
                if(!exemplary) {
                    res.status(404) ;
                    res.json({
                        msg: 'exemplary not found',
                        id_exemplary: exemplary_id
                    });
                    return next();
                }

                res.json({
                    msg: 'exemplary found',
                    exemplary: exemplary,
                });
                return next();
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during exemplary researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {put} exemplary/:id Update
     * @apiName update
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     *
     * @apiParam {String} [label] label of exemplary
     * @apiParam {String} [location] location_id of exemplary
     * @apiParam {String} [item] label_id of exemplary
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {Objet} exemplary exemplary object upgraded
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull upgrad",
     *           "exemplary": {
     *               "label": null,
     *               "modified": "2020-06-01T22:49:23.933Z",
     *               "created": "2020-05-29T18:20:17.575Z",
     *               "_id": "5ed152a623ade50988ff9f9d",
     *               "location": {
     *                   "quantity_max": -1,
     *                   "total_exemplaries": 2,
     *                   "total_items": 1,
     *                   "modified": "2020-05-29T18:07:41.157Z",
     *                   "created": "2020-05-29T18:07:41.157Z",
     *                   "_id": "5ed1501d897732197421a6e6",
     *                   "label": "etage 4",
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               "item": {
     *                   "author": "patrick",
     *                   "editor": "maplplzodzdz",
     *                   "type": "book",
     *                   "date_of_publish": "plzoplzo",
     *                   "total_exemplaries": 2,
     *                   "path_img": "/test",
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "category": null,
     *                   "label": " dzadaz",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-05-29T18:10:01.249Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg": "error occured during exemplary creation"
     *          "error": "error"
     *     }
     */
    public update(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        ExemplaryModel.findOne({'_id':exemplary_id, library_id:token.library_id})
            .then((exemplary: any) => {
                if(!exemplary) {
                    res.status(404);
                    res.json({
                        msg: 'exemplary not found',
                        id_exemplary: exemplary_id
                    });
                    return next();
                }
                if(req.body.location && req.body.location != exemplary.location){
                    LocationModel.updateQuantity(exemplary.location, -1,token.library_id).then(
                        LocationModel.updateQuantity(req.body.location, +1, token.library_id)
                    );
                }
                exemplary.dynamicUpdate(req.body)
                    .then((exemplary: any) => {
                        res.json({
                            msg: 'successfull upgrad',
                            exemplarys: exemplary,
                        });
                        return next();
                    })
                    .catch((error: any)=> {
                        res.status(500);
                        res.json({
                            msg: 'error occured during update',
                            error: error
                        });
                        return next([error]);
                    });
            })
            .catch((error: any)=> {
                res.status(500);
                res.json({
                    msg: 'error occured during exemplary researched',
                    error: error
                });
                return next([error]);
            });
    }

    /**
     * @api {delete} exemplary/:id Delete
     * @apiName delete
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     *
     * @apiSuccess {String} msg successfull upgrad
     * @apiSuccess {String} total_deleted number of categories deleted
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "successfull suppression",
     *           "total_deleted": 1
     *       }
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public delete(req: Request, res: Response, next: any) {
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        ExemplaryModel.delete(exemplary_id, token.library_id)
            .then(exemplary => {
                res.json({
                    msg:'exemplary successfully deleted',
                    exemplary: exemplary
                })
            })
            .catch((error: any) => {
                if(error === 'exemplary not found'){
                    res.status(404)
                }
                else{
                    res.status(500);
                }
                res.json({
                    msg: 'error occured during exemplary suppression',
                    error: error
                });
                return next([error]);
            });
    }


    /**
     * @api {post} exemplary/:id/lend Lent
     * @apiName lend
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     * @apiDescription lent one exemplary
     *
     * @apiParam {String} receiver_name the receiver of the exemplary
     * @apiParam {String} lend_date the date the receiver should recover the exemplary
     * @apiParam {String} [note] note about the lend
     *
     * @apiSuccess {String} msg exemplary successfully lend
     * @apiSuccess {Objet} exemplary exemplary lend
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "exemplary successfully lend",
     *           "exemplary": {
     *               "label": null,
     *               "modified": "2020-06-01T22:49:23.933Z",
     *               "created": "2020-05-29T18:20:17.575Z",
     *               "_id": "5ed152a623ade50988ff9f9d",
     *               "location": {
     *                   "quantity_max": -1,
     *                   "total_exemplaries": 2,
     *                   "total_items": 1,
     *                   "modified": "2020-05-29T18:07:41.157Z",
     *                   "created": "2020-05-29T18:07:41.157Z",
     *                   "_id": "5ed1501d897732197421a6e6",
     *                   "label": "etage 4",
     *                   "library_id": "5ed14fc3897732197421a6e3"
     *               },
     *               "item": {
     *                   "author": "Author test update",
     *                   "editor": "Editor test update",
     *                   "type": "Type test update",
     *                   "date_of_publish": "plzoplzo",
     *                   "total_exemplaries": 2,
     *                   "path_img": "/test",
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "category": null,
     *                   "label": "Label test update ",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-06-01T22:52:47.582Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3",
     *               "lend_information": {
     *                   "receiver_name": "etst",
     *                   "note": "undefined",
     *                   "date": 5000
     *               }
     *           }
     *       }
     *
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     * @apiError (409) ExemplaryAlreadyLend The exemplary is already lend
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 409 ExemplaryAlreadyLend
     *       {
     *           "msg": "Unable to lent this exemplary",
     *           "error": "This exemplary is already lend"
     *       }
     *
     * @apiError (400) ArgumentMissing Mandatory arguments missing
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 ArgumentMissing
     *       {
     *          "msg": mandatory arguments missing. need at least receiver_name and lend_date
     *       }
     *
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public lend(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        let receiver_name = String(req.body.receiver_name) ?? null;
        let lend_note = String(req.body.lend_note) ?? null;
        let lend_date = Number(req.body.lend_date) ?? null;
        if(lend_date && receiver_name) {
            ExemplaryModel.lend(exemplary_id, receiver_name, lend_note, lend_date, token.library_id)
                .then(exemplary => {
                    res.json({
                        msg: 'exemplary successfully lend',
                        exemplary: exemplary
                    })
                })
                .catch(error => {
                    if (error === 'This exemplary is already lend') {
                        res.status(409);
                        res.json({
                            msg: 'Unable to lent this exemplary',
                            error: error.error,
                            exemplary: error.exemplary
                        })
                        return next([error]);
                    }
                    res.status(500);
                    res.json({
                        msg: 'Unable to lent this exemplary',
                        error: error.error
                    });
                    return next([error]);
                })
        }
        else{
            res.status(400);
            res.json({
                msg: 'mandatory arguments missing. need at least receiver_name and lend_date',
            })
        }

    }


    /**
     * @api {post} exemplary/:id/recover Recover
     * @apiName recovery
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     * @apiDescription recover one exemplary
     * @apiParam {String} location_id the new location of exemplary
     *
     * @apiSuccess {String} msg exemplary successfully lend
     * @apiSuccess {Objet} exemplary exemplary lend
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "exemplary successfully recovered",
     *           "exemplary": {
     *               "label": null,
     *               "modified": "2020-06-01T22:49:23.933Z",
     *               "created": "2020-05-29T18:20:17.575Z",
     *               "_id": "5ed152a623ade50988ff9f9d",
     *               "location": "5ec9473f8647540017503242",
     *               "item": {
     *                   "author": "Author test update",
     *                   "editor": "Editor test update",
     *                   "type": "Type test update",
     *                   "date_of_publish": "plzoplzo",
     *                   "total_exemplaries": 2,
     *                   "path_img": "/test",
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "category": null,
     *                   "label": "Label test update ",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-06-01T22:52:47.582Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     * @apiError (409) ExemplaryNotLend The exemplary is not lend
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 409 ExemplaryNotLend
     *       {
     *           "msg": "Unable to recover this exemplary",
     *           "error": "This exemplary is not actually lend"
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public recovery(req: Request, res: Response, next: any){
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        let location_id = req.body.location_id;
        ExemplaryModel.recovery(exemplary_id,location_id, token.library_id)
            .then(exemplary => {
                res.json({
                    msg: 'exemplary successfully recovered',
                    exemplary: exemplary
                })
            })
            .catch(error => {
                if (error === 'This exemplary is not actually lend') {
                    res.status(409);
                    res.json({
                        msg: 'Unable to recover this exemplary',
                        error: error.error,
                        exemplary: error.exemplary
                    })
                    return next([error]);
                }
                res.status(500);
                res.json({
                    msg: 'Unable to recover this exemplary',
                    error: error.error
                });
                return next([error]);
            })
    }

    /**
     * @api {post} exemplary/:id/move Move
     * @apiName recovery
     * @apiGroup Exemplary
     * @apiPermission Token-Full
     * @apiDescription move one exemplary to an another location
     *
     * @apiParam {String} new_location the new location of exemplary
     *
     * @apiSuccess {String} msg exemplary successfully lend
     * @apiSuccess {Objet} exemplary exemplary lend
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *       {
     *           "msg": "move successfull ",
     *           "exemplary": {
     *               "label": "Four shadow Exemplary 1 ",
     *               "modified": "2020-06-01T23:31:22.154Z",
     *               "created": "2020-06-01T23:31:22.154Z",
     *               "_id": "5ed58ffa7c676c07d4a0b795",
     *               "location": null,
     *               "item": {
     *                   "author": "Author test update",
     *                   "editor": "Editor test update",
     *                   "type": "Type test update",
     *                   "date_of_publish": "2020-10-04",
     *                   "total_exemplaries": 3,
     *                   "path_img": "/test",
     *                   "_id": "5ed14ff9897732197421a6e5",
     *                   "category": null,
     *                   "label": "Four Shadow",
     *                   "library_id": "5ed14fc3897732197421a6e3",
     *                   "modified": "2020-06-01T22:52:47.582Z",
     *                   "created": "2020-05-29T18:10:01.249Z"
     *               },
     *               "library_id": "5ed14fc3897732197421a6e3"
     *           }
     *       }
     *
     * @apiError (404) ExemplaryNotFound The <code>id</code> of the Exemplary was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 ExemplaryNotFound
     *     {
     *          "msg": "exemplary not found"
     *          "exemplary_id": 5ed151de16ab5e26109cd068
     *     }
     *
     * @apiError (409) SameLocation The new location is the same that the current location
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 409 SameLocation
     *       {
     *           "msg": current location_id is the same that new_location
     *       }
     *
     * @apiError (500) InternalServerError An unknown error occured
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 (500) InternalServerError An unknown error occured
     *     {
     *          "msg":  "error occured during exemplary researched"
     *          "error": "error"
     *     }
     */
    public move(req: Request, res: Response, next: any) {
        //@ts-ignore
        let token = req.user;
        let exemplary_id = req.params.exemplary_id;
        let new_location = req.body.new_location ?? null;
        if(!new_location){
            res.status(400);
            res.json({
                msg: 'mandatory arguments missing. need : new_location',
            })
            return next();
        }
        ExemplaryModel.retrieveExemplaries(null,null,{'_id':exemplary_id},null,false, token.library_id)
            .then(exemplaries => {
                if(exemplaries.length === 0 ){
                    res.status(404);
                    res.json({
                        msg: 'exemplary not found',
                        exemplary_id: exemplary_id
                    })
                    return next();
                }
                exemplaries[0].move(new_location, token.library_id)
                    .then(exemplary => {
                        res.json({
                            msg: 'move successfull ',
                            exemplary: exemplary
                        })
                    })
                    .catch(error => {
                        console.log(error);
                        if(error.error === 'current location_id is the same that new_location'){
                            res.status(409);
                            res.json({
                                msg: 'current location_id is the same that new_location',
                                exemplary:error.exemplary,
                                error: error.error
                            })
                        }
                        else {
                            res.status(500);
                            res.json({
                                msg: 'move was not done',
                                error: error
                            })
                        }
                    })

            })
    }


}
