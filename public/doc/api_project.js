define({
  "title": "API DOC",
  "url": "https://api-library-ter.herokuapp.com/",
  "name": "Stockme",
  "version": "1.4.22",
  "description": "RESTFull API used for mobile libraries application",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-06-02T16:37:53.337Z",
    "url": "http://apidocjs.com",
    "version": "0.23.0"
  }
});
