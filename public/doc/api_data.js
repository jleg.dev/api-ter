define({ "api": [
  {
    "type": "delete",
    "url": "category/:id",
    "title": "Delete",
    "name": "delete",
    "group": "Category",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_deleted",
            "description": "<p>number of categories deleted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull suppression\",\n      \"total_deleted\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The <code>id</code> of the Category was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 CategoryNotFound\n{\n     \"msg\": \"category not found\"\n     \"category_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during category researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/categoryController.ts",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "category",
    "title": "Create",
    "name": "insert",
    "group": "Category",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>label of category</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successful insertion</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_category",
            "description": "<p>id of category created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category_created",
            "description": "<p>category created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successful insertion\",\n      \"id_category\": \"5ed581089e689f18c0b5695f\",\n      \"category_created\": {\n          \"modified\": \"2020-06-01T22:28:14.836Z\",\n          \"created\": \"2020-06-01T22:28:14.836Z\",\n          \"_id\": \"5ed581089e689f18c0b5695f\",\n          \"label\": \"other\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during category creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/categoryController.ts",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "category",
    "title": "Request all",
    "name": "list",
    "group": "Category",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of categories retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter categories on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of categories returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>categories successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "categories",
            "description": "<p>list of categories</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n \"total_returned\": 1,\n \"total_page\": 1,\n \"msg\": \"categories successfully retrieved\",\n \"categories\": [\n      {\n          \"_id\": \"5ed151de16ab5e26109cd068\",\n          \"modified\": \"2020-05-29T18:17:41.744Z\",\n          \"created\": \"2020-05-29T18:17:41.744Z\",\n          \"label\": \"other\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/categoryController.ts",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "category/:id",
    "title": "Request one",
    "name": "show",
    "group": "Category",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>locations successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>category found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"category found\",\n      \"category\": {\n          \"_id\": \"5ed151de16ab5e26109cd068\",\n          \"modified\": \"2020-05-29T18:17:41.744Z\",\n          \"created\": \"2020-05-29T18:17:41.744Z\",\n          \"label\": \"other\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The <code>id</code> of the Category was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 CategoryNotFound\n{\n     \"msg\": \"category not found\"\n     \"category_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during category researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/categoryController.ts",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "category/:id",
    "title": "Update",
    "name": "update",
    "group": "Category",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "label",
            "description": "<p>label of category</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "category",
            "description": "<p>category object upgraded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull upgrad\",\n      \"category\": {\n          \"modified\": \"2020-06-01T22:48:03.770Z\",\n          \"created\": \"2020-05-29T18:17:41.744Z\",\n          \"_id\": \"5ed151de16ab5e26109cd068\",\n          \"label\": \"fraise\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The <code>id</code> of the Category was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 CategoryNotFound\n{\n     \"msg\": \"category not found\"\n     \"category_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": error occured during category creation\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/categoryController.ts",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "exemplary/:id",
    "title": "Delete",
    "name": "delete",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_deleted",
            "description": "<p>number of categories deleted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull suppression\",\n      \"total_deleted\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "post",
    "url": "exemplary",
    "title": "Create",
    "name": "insert",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>label of exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>location_id of exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "item",
            "description": "<p>label_id of exemplary</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successful insertion</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_exemplary",
            "description": "<p>id of exemplary created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "exemplary_created",
            "description": "<p>exemplary created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successful insertion\",\n      \"id_exemplary\": \"5ed5839e81d5292798e061af\",\n      \"exemplary_created\": {\n          \"label\": null,\n          \"modified\": \"2020-06-01T22:39:25.066Z\",\n          \"created\": \"2020-06-01T22:39:25.066Z\",\n          \"_id\": \"5ed5839e81d5292798e061af\",\n          \"location\": {\n              \"quantity_max\": -1,\n              \"total_exemplaries\": 1,\n              \"total_items\": 1,\n              \"modified\": \"2020-05-29T18:07:41.157Z\",\n              \"created\": \"2020-05-29T18:07:41.157Z\",\n              \"_id\": \"5ed1501d897732197421a6e6\",\n              \"label\": \"etage 4\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          \"item\": {\n              \"author\": \"patrick\",\n              \"editor\": \"maplplzodzdz\",\n              \"type\": \"book\",\n              \"date_of_publish\": \"plzoplzo\",\n              \"total_exemplaries\": 1,\n              \"path_img\": \"/test\",\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"category\": null,\n              \"label\": \" dzadaz\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-05-29T18:10:01.249Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during exemplary creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "post",
    "url": "exemplary/:id/lend",
    "title": "Lent",
    "name": "lend",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>lent one exemplary</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "receiver_name",
            "description": "<p>the receiver of the exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lend_date",
            "description": "<p>the date the receiver should recover the exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "note",
            "description": "<p>note about the lend</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplary successfully lend</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "exemplary",
            "description": "<p>exemplary lend</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"exemplary successfully lend\",\n      \"exemplary\": {\n          \"label\": null,\n          \"modified\": \"2020-06-01T22:49:23.933Z\",\n          \"created\": \"2020-05-29T18:20:17.575Z\",\n          \"_id\": \"5ed152a623ade50988ff9f9d\",\n          \"location\": {\n              \"quantity_max\": -1,\n              \"total_exemplaries\": 2,\n              \"total_items\": 1,\n              \"modified\": \"2020-05-29T18:07:41.157Z\",\n              \"created\": \"2020-05-29T18:07:41.157Z\",\n              \"_id\": \"5ed1501d897732197421a6e6\",\n              \"label\": \"etage 4\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          \"item\": {\n              \"author\": \"Author test update\",\n              \"editor\": \"Editor test update\",\n              \"type\": \"Type test update\",\n              \"date_of_publish\": \"plzoplzo\",\n              \"total_exemplaries\": 2,\n              \"path_img\": \"/test\",\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"category\": null,\n              \"label\": \"Label test update \",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-06-01T22:52:47.582Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\",\n          \"lend_information\": {\n              \"receiver_name\": \"etst\",\n              \"note\": \"undefined\",\n              \"date\": 5000\n          }\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "ArgumentMissing",
            "description": "<p>Mandatory arguments missing</p>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "409": [
          {
            "group": "409",
            "optional": false,
            "field": "ExemplaryAlreadyLend",
            "description": "<p>The exemplary is already lend</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 ExemplaryAlreadyLend\n  {\n      \"msg\": \"Unable to lent this exemplary\",\n      \"error\": \"This exemplary is already lend\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 ArgumentMissing\n  {\n     \"msg\": mandatory arguments missing. need at least receiver_name and lend_date\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "get",
    "url": "exemplary",
    "title": "Request all",
    "name": "list",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of exemplaries retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter exemplaries on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of exemplaries returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplaries successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "exemplaries",
            "description": "<p>list of exemplaries</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n \"total_returned\": 1,\n \"total_page\": 1,\n \"msg\": \"exemplaries successfully retrieved\"\n \"exemplaries\": [\n  {\n         \"_id\": \"5ed152a623ade50988ff9f9d\",\n         \"label\": null,\n         \"modified\": \"2020-05-29T18:20:17.575Z\",\n         \"created\": \"2020-05-29T18:20:17.575Z\",\n         \"location\": {\n             \"_id\": \"5ed1501d897732197421a6e6\",\n             \"quantity_max\": -1,\n             \"total_exemplaries\": 1,\n             \"total_items\": 1,\n             \"modified\": \"2020-05-29T18:07:41.157Z\",\n             \"created\": \"2020-05-29T18:07:41.157Z\",\n             \"label\": \"etage 4\",\n             \"library_id\": \"5ed14fc3897732197421a6e3\"\n         },\n         \"item\": {\n             \"_id\": \"5ed14ff9897732197421a6e5\",\n             \"author\": \"patrick\",\n             \"editor\": \"maplplzodzdz\",\n             \"type\": \"book\",\n             \"date_of_publish\": \"plzoplzo\",\n             \"total_exemplaries\": 1,\n             \"path_img\": \"/test\",\n             \"category\": null,\n             \"label\": \" dzadaz\",\n             \"library_id\": \"5ed14fc3897732197421a6e3\",\n             \"modified\": \"2020-05-29T18:10:01.249Z\",\n             \"created\": \"2020-05-29T18:10:01.249Z\"\n         },\n         \"library_id\": \"5ed14fc3897732197421a6e3\"\n   }]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "post",
    "url": "exemplary/:id/recover",
    "title": "Recover",
    "name": "recovery",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>recover one exemplary</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location_id",
            "description": "<p>the new location of exemplary</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplary successfully lend</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "exemplary",
            "description": "<p>exemplary lend</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"exemplary successfully recovered\",\n      \"exemplary\": {\n          \"label\": null,\n          \"modified\": \"2020-06-01T22:49:23.933Z\",\n          \"created\": \"2020-05-29T18:20:17.575Z\",\n          \"_id\": \"5ed152a623ade50988ff9f9d\",\n          \"location\": \"5ec9473f8647540017503242\",\n          \"item\": {\n              \"author\": \"Author test update\",\n              \"editor\": \"Editor test update\",\n              \"type\": \"Type test update\",\n              \"date_of_publish\": \"plzoplzo\",\n              \"total_exemplaries\": 2,\n              \"path_img\": \"/test\",\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"category\": null,\n              \"label\": \"Label test update \",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-06-01T22:52:47.582Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "409": [
          {
            "group": "409",
            "optional": false,
            "field": "ExemplaryNotLend",
            "description": "<p>The exemplary is not lend</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 ExemplaryNotLend\n  {\n      \"msg\": \"Unable to recover this exemplary\",\n      \"error\": \"This exemplary is not actually lend\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "post",
    "url": "exemplary/:id/move",
    "title": "Move",
    "name": "recovery",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>move one exemplary to an another location</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_location",
            "description": "<p>the new location of exemplary</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplary successfully lend</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "exemplary",
            "description": "<p>exemplary lend</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"move successfull \",\n      \"exemplary\": {\n          \"label\": \"Four shadow Exemplary 1 \",\n          \"modified\": \"2020-06-01T23:31:22.154Z\",\n          \"created\": \"2020-06-01T23:31:22.154Z\",\n          \"_id\": \"5ed58ffa7c676c07d4a0b795\",\n          \"location\": null,\n          \"item\": {\n              \"author\": \"Author test update\",\n              \"editor\": \"Editor test update\",\n              \"type\": \"Type test update\",\n              \"date_of_publish\": \"2020-10-04\",\n              \"total_exemplaries\": 3,\n              \"path_img\": \"/test\",\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"category\": null,\n              \"label\": \"Four Shadow\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-06-01T22:52:47.582Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "409": [
          {
            "group": "409",
            "optional": false,
            "field": "SameLocation",
            "description": "<p>The new location is the same that the current location</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 409 SameLocation\n  {\n      \"msg\": current location_id is the same that new_location\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "get",
    "url": "exemplary/:id",
    "title": "Request one",
    "name": "show",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>locations successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "exemplary",
            "description": "<p>exemplary found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"exemplary found\",\n      \"exemplary\": {\n          \"_id\": \"5ed152a623ade50988ff9f9d\",\n          \"label\": null,\n          \"modified\": \"2020-05-29T18:20:17.575Z\",\n          \"created\": \"2020-05-29T18:20:17.575Z\",\n          \"location\": {\n              \"_id\": \"5ed1501d897732197421a6e6\",\n              \"quantity_max\": -1,\n              \"total_exemplaries\": 1,\n              \"total_items\": 1,\n              \"modified\": \"2020-05-29T18:07:41.157Z\",\n              \"created\": \"2020-05-29T18:07:41.157Z\",\n              \"label\": \"etage 4\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          \"item\": {\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"author\": \"patrick\",\n              \"editor\": \"maplplzodzdz\",\n              \"type\": \"book\",\n              \"date_of_publish\": \"plzoplzo\",\n              \"total_exemplaries\": 1,\n              \"path_img\": \"/test\",\n              \"category\": null,\n              \"label\": \" dzadaz\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-05-29T18:10:01.249Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "put",
    "url": "exemplary/:id",
    "title": "Update",
    "name": "update",
    "group": "Exemplary",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "label",
            "description": "<p>label of exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "location",
            "description": "<p>location_id of exemplary</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "item",
            "description": "<p>label_id of exemplary</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "exemplary",
            "description": "<p>exemplary object upgraded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull upgrad\",\n      \"exemplary\": {\n          \"label\": null,\n          \"modified\": \"2020-06-01T22:49:23.933Z\",\n          \"created\": \"2020-05-29T18:20:17.575Z\",\n          \"_id\": \"5ed152a623ade50988ff9f9d\",\n          \"location\": {\n              \"quantity_max\": -1,\n              \"total_exemplaries\": 2,\n              \"total_items\": 1,\n              \"modified\": \"2020-05-29T18:07:41.157Z\",\n              \"created\": \"2020-05-29T18:07:41.157Z\",\n              \"_id\": \"5ed1501d897732197421a6e6\",\n              \"label\": \"etage 4\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          \"item\": {\n              \"author\": \"patrick\",\n              \"editor\": \"maplplzodzdz\",\n              \"type\": \"book\",\n              \"date_of_publish\": \"plzoplzo\",\n              \"total_exemplaries\": 2,\n              \"path_img\": \"/test\",\n              \"_id\": \"5ed14ff9897732197421a6e5\",\n              \"category\": null,\n              \"label\": \" dzadaz\",\n              \"library_id\": \"5ed14fc3897732197421a6e3\",\n              \"modified\": \"2020-05-29T18:10:01.249Z\",\n              \"created\": \"2020-05-29T18:10:01.249Z\"\n          },\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ExemplaryNotFound",
            "description": "<p>The <code>id</code> of the Exemplary was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ExemplaryNotFound\n{\n     \"msg\": \"exemplary not found\"\n     \"exemplary_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during exemplary creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/exemplaryController.ts",
    "groupTitle": "Exemplary"
  },
  {
    "type": "delete",
    "url": "item/:id",
    "title": "Delete",
    "name": "delete",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>An item should not refer to existent exemplaries to be removed</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_deleted",
            "description": "<p>number of categories deleted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull suppression\",\n      \"total_deleted\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ItemNotFound\n{\n     \"msg\": \"item not found\"\n     \"item_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during item researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "post",
    "url": "item",
    "title": "Create",
    "name": "insert",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>label of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "author",
            "description": "<p>author of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "editor",
            "description": "<p>editor of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>type of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "date_of_publish",
            "description": "<p>date of publication of  item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "isbn",
            "description": "<p>isbn of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>category_id of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path_img",
            "description": "<p>path of image of item</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successful insertion</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_item",
            "description": "<p>id of item created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "item_created",
            "description": "<p>item created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successful insertion\",\n      \"id_item\": \"5ed584043e24cd285cce0681\",\n      \"item_created\": {\n          \"author\": \"Patrick Descamps\",\n          \"editor\": \"Flammarion\",\n          \"type\": \"Book\",\n          \"date_of_publish\": \"2020-04-05\",\n          \"total_exemplaries\": 0,\n          \"path_img\": \"http://google.image.fr/fourshadow.png\",\n          \"_id\": \"5ed584043e24cd285cce0681\",\n          \"category\": null,\n          \"label\": \"Four shadows\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\",\n          \"modified\": \"2020-06-01T22:41:08.841Z\",\n          \"created\": \"2020-06-01T22:41:08.841Z\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during item creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "get",
    "url": "item",
    "title": "Request all",
    "name": "list",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of items retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter items on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of items returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>items successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "items",
            "description": "<p>list of items</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n \"total_returned\": 1,\n \"total_page\": 1,\n \"msg\": \"items successfully retrieved\",\n \"items\": [\n {\n     \"total_returned\": 1,\n     \"total_page\": 1,\n     \"msg\": \"items successfully retrieved\",\n     \"items\": [\n      {\n         \"_id\": \"5ed14ff9897732197421a6e5\",\n         \"author\": \"patrick\",\n         \"editor\": \"maplplzodzdz\",\n         \"type\": \"book\",\n         \"date_of_publish\": \"plzoplzo\",\n         \"total_exemplaries\": 1,\n         \"path_img\": \"/test\",\n         \"category\": null,\n         \"label\": \" dzadaz\",\n         \"library_id\": \"5ed14fc3897732197421a6e3\",\n         \"modified\": \"2020-05-29T18:10:01.249Z\",\n         \"created\": \"2020-05-29T18:10:01.249Z\"\n     }\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "get",
    "url": "item/:id/exemplary",
    "title": "Retrieve exemplaries of item",
    "name": "listExemplaries",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of items retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter items on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplaries successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "exemplaries",
            "description": "<p>list of exemplaries</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_exemplaries",
            "description": "<p>total exemplaries of current item</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of items returned</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"exemplaries successfully retrieved\",\n      \"exemplaries\": [\n          {\n              \"label\": null,\n              \"modified\": \"2020-06-01T22:49:23.933Z\",\n              \"created\": \"2020-05-29T18:20:17.575Z\",\n              \"_id\": \"5ed152a623ade50988ff9f9d\",\n              \"item\": {\n                  \"quantity_max\": -1,\n                  \"total_exemplaries\": 2,\n                  \"total_items\": 1,\n                  \"modified\": \"2020-05-29T18:07:41.157Z\",\n                  \"created\": \"2020-05-29T18:07:41.157Z\",\n                  \"_id\": \"5ed1501d897732197421a6e6\",\n                  \"label\": \"etage 4\",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\"\n              },\n              \"item\": {\n                  \"author\": \"Author test update\",\n                  \"editor\": \"Editor test update\",\n                  \"type\": \"Type test update\",\n                  \"date_of_publish\": \"plzoplzo\",\n                  \"total_exemplaries\": 2,\n                  \"path_img\": \"/test\",\n                  \"_id\": \"5ed14ff9897732197421a6e5\",\n                  \"category\": null,\n                  \"label\": \"Label test update \",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\",\n                  \"modified\": \"2020-06-01T22:52:47.582Z\",\n                  \"created\": \"2020-05-29T18:10:01.249Z\"\n              },\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          {\n              \"label\": null,\n              \"modified\": \"2020-06-01T22:39:25.066Z\",\n              \"created\": \"2020-06-01T22:39:25.066Z\",\n              \"_id\": \"5ed5839e81d5292798e061af\",\n              \"item\": {\n                  \"quantity_max\": -1,\n                  \"total_exemplaries\": 2,\n                  \"total_items\": 1,\n                  \"modified\": \"2020-05-29T18:07:41.157Z\",\n                  \"created\": \"2020-05-29T18:07:41.157Z\",\n                  \"_id\": \"5ed1501d897732197421a6e6\",\n                  \"label\": \"etage 4\",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\"\n              },\n              \"item\": {\n                  \"author\": \"Author test update\",\n                  \"editor\": \"Editor test update\",\n                  \"type\": \"Type test update\",\n                  \"date_of_publish\": \"plzoplzo\",\n                  \"total_exemplaries\": 2,\n                  \"path_img\": \"/test\",\n                  \"_id\": \"5ed14ff9897732197421a6e5\",\n                  \"category\": null,\n                  \"label\": \"Label test update \",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\",\n                  \"modified\": \"2020-06-01T22:52:47.582Z\",\n                  \"created\": \"2020-05-29T18:10:01.249Z\"\n              },\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          }\n      ],\n      \"total_exemplaries\": 2,\n      \"total_page\": 1,\n      \"total_returned\": 2\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ItemNotFound\n{\n     \"msg\": \"item not found\"\n     \"item_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during item researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "get",
    "url": "item/:id",
    "title": "Request one",
    "name": "show",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>locations successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "item",
            "description": "<p>item found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"item found\",\n      \"item\": {\n          \"_id\": \"5ed14ff9897732197421a6e5\",\n          \"author\": \"patrick\",\n          \"editor\": \"maplplzodzdz\",\n          \"type\": \"book\",\n          \"date_of_publish\": \"plzoplzo\",\n          \"total_exemplaries\": 1,\n          \"path_img\": \"/test\",\n          \"category\": null,\n          \"label\": \" dzadaz\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\",\n          \"modified\": \"2020-05-29T18:10:01.249Z\",\n          \"created\": \"2020-05-29T18:10:01.249Z\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ItemNotFound\n{\n     \"msg\": \"item not found\"\n     \"item_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during item researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "put",
    "url": "item/:id",
    "title": "Update",
    "name": "update",
    "group": "Item",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "label",
            "description": "<p>label of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "author",
            "description": "<p>author of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "editor",
            "description": "<p>editor of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "type",
            "description": "<p>type] of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "date_of_publish",
            "description": "<p>date of publication of  item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "isbn",
            "description": "<p>isbn] of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "category",
            "description": "<p>category_id of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "path_img",
            "description": "<p>path of image of item</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "item",
            "description": "<p>item object upgraded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull upgrad\",\n      \"item\": {\n          \"author\": \"Author test update\",\n          \"editor\": \"Editor test update\",\n          \"type\": \"Type test update\",\n          \"date_of_publish\": \"plzoplzo\",\n          \"total_exemplaries\": 2,\n          \"path_img\": \"/test\",\n          \"_id\": \"5ed14ff9897732197421a6e5\",\n          \"category\": null,\n          \"label\": \"Label test update \",\n          \"library_id\": \"5ed14fc3897732197421a6e3\",\n          \"modified\": \"2020-06-01T22:52:47.582Z\",\n          \"created\": \"2020-05-29T18:10:01.249Z\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "ItemNotFound",
            "description": "<p>The <code>id</code> of the Item was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 ItemNotFound\n{\n     \"msg\": \"item not found\"\n     \"item_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during item creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/itemController.ts",
    "groupTitle": "Item"
  },
  {
    "type": "post",
    "url": "library",
    "title": "Create",
    "name": "insert",
    "group": "Library",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of library</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>address of library</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "path_img",
            "description": "<p>path image of library</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successful insertion</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_library",
            "description": "<p>id of library created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "library_created",
            "description": "<p>library created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"library successfully created\",\n      \"library\": {\n          \"modified\": \"2020-06-01T22:42:39.950Z\",\n          \"created\": \"2020-06-01T22:42:39.950Z\",\n          \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTEwNTEzNzEsImV4cCI6MTU5MTY1NjE3MX0._KzLpR6SRJUDA-fPQ8KCfJjwMMpAKm82gigI9-O4cmQ\",\n          \"path_img\": null,\n          \"_id\": \"5ed5846ba1a59c2c204f8441\",\n          \"name\": \"dzdze\",\n          \"address\": \"daz\",\n          \"workers\": [\n              {\n                  \"role\": \"admin\",\n                  \"_id\": \"5ed5846ba1a59c2c204f8442\",\n                  \"worker\": \"5ed14f8c897732197421a6e2\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during library creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/libraryController.ts",
    "groupTitle": "Library"
  },
  {
    "type": "get",
    "url": "library",
    "title": "Request all",
    "name": "list",
    "group": "Library",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of libraries retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter libraries on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of libraries returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>libraries successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "libraries",
            "description": "<p>list of libraries</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"library of current user successfully retrieved\",\n      \"libraries\": [\n          {\n              \"modified\": \"2020-05-29T18:07:40.853Z\",\n              \"created\": \"2020-05-29T18:07:40.853Z\",\n              \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTA3NzU3NDcsImV4cCI6MTU5MTM4MDU0N30.glCQAmwCdh8Y6BPEefLLnK_GyXkWYbVyGzepTkXQZk4\",\n              \"path_img\": null,\n              \"_id\": \"5ed14fc3897732197421a6e3\",\n              \"name\": \"dzdze\",\n              \"address\": \"daz\",\n              \"workers\": [\n                  {\n                      \"role\": \"admin\",\n                      \"_id\": \"5ed14fc3897732197421a6e4\",\n                      \"worker\": \"5ed14f8c897732197421a6e2\"\n                  }\n              ]\n          }\n      ],\n      \"total_returned\": 1,\n      \"total_page\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/libraryController.ts",
    "groupTitle": "Library"
  },
  {
    "type": "get",
    "url": "/library_users_list",
    "title": "Request all users",
    "name": "listUsers",
    "group": "Library",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>Retrieve all users of current library. Possible only for admin of the library</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of libraries retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter libraries on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of libraries returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>users successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "users",
            "description": "<p>list of users</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"users successfully retrieved\",\n      \"users\": [\n          {\n              \"role\": \"admin\",\n              \"_id\": \"5ed14fc3897732197421a6e4\",\n              \"worker\": {\n                  \"_id\": \"5ed14f8c897732197421a6e2\",\n                  \"email\": \"azerat@a.com\",\n                  \"last_name\": \"eaz\",\n                  \"first_name\": \"eaze\"\n              }\n          }\n      ],\n      \"total_returned\": 1,\n      \"total_page\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "403": [
          {
            "group": "403",
            "optional": false,
            "field": "NotAutorized",
            "description": "<p>Not autorized to effectuate this action</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action\n{\n     \"msg\": \"user not autorized to get the list of users of the current library\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/libraryController.ts",
    "groupTitle": "Library"
  },
  {
    "type": "put",
    "url": "library",
    "title": "Update",
    "name": "update",
    "group": "Library",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>name of library</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>address of library*</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "path_img",
            "description": "<p>path image of library</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "library",
            "description": "<p>library object upgraded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull upgrad\",\n      \"library\": {\n          \"modified\": \"2020-06-01T22:57:53.952Z\",\n          \"created\": \"2020-05-29T18:07:40.853Z\",\n          \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1OTA3NzU3NDcsImV4cCI6MTU5MTM4MDU0N30.glCQAmwCdh8Y6BPEefLLnK_GyXkWYbVyGzepTkXQZk4\",\n          \"path_img\": \"dazdza\",\n          \"_id\": \"5ed14fc3897732197421a6e3\",\n          \"name\": \"dzdze\",\n          \"address\": \"daz\",\n          \"workers\": [\n              {\n                  \"role\": \"admin\",\n                  \"_id\": \"5ed14fc3897732197421a6e4\",\n                  \"worker\": \"5ed14f8c897732197421a6e2\"\n              }\n          ]\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "LibraryNotFound",
            "description": "<p>The <code>id</code> of the Library was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 LibraryNotFound\n{\n     \"msg\": \"library not found\"\n     \"library_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during library creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/libraryController.ts",
    "groupTitle": "Library"
  },
  {
    "type": "delete",
    "url": "location/:id",
    "title": "Delete",
    "name": "delete",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "description": "<p>A location should be empty to be removed</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_deleted",
            "description": "<p>number of categories deleted</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull suppression\",\n      \"total_deleted\": 1\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "LocationNotFound",
            "description": "<p>The <code>id</code> of the Location was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 LocationNotFound\n{\n     \"msg\": \"location not found\"\n     \"location_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during location researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "post",
    "url": "location",
    "title": "Create",
    "name": "insert",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>address of location</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successful insertion</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id_location",
            "description": "<p>id of location created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "location_created",
            "description": "<p>location created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successful insertion\",\n      \"id_location\": \"5ed584b2d37fc81c5484e9b4\",\n      \"location_created\": {\n          \"quantity_max\": -1,\n          \"total_exemplaries\": 0,\n          \"total_items\": 0,\n          \"modified\": \"2020-06-01T22:44:01.474Z\",\n          \"created\": \"2020-06-01T22:44:01.474Z\",\n          \"_id\": \"5ed584b2d37fc81c5484e9b4\",\n          \"label\": \"etage 4\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during location creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "get",
    "url": "location",
    "title": "Request all",
    "name": "list",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of locations retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter locations on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of locations returned</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>locations successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "locations",
            "description": "<p>list of locations</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"total_returned\": 1,\n      \"total_page\": 1,\n      \"msg\": \"location successfully retrieved\",\n      \"locations\": [\n          {\n              \"_id\": \"5ed1501d897732197421a6e6\",\n              \"quantity_max\": -1,\n              \"total_exemplaries\": 1,\n              \"total_items\": 1,\n              \"modified\": \"2020-05-29T18:07:41.157Z\",\n              \"created\": \"2020-05-29T18:07:41.157Z\",\n              \"label\": \"etage 4\",\n              \"location_id\": \"5ed14fc3897732197421a6e3\"\n          }\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during document count / research\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "get",
    "url": "location/:id/exemplary",
    "title": "Retrieve exemplaries",
    "name": "listExemplaries",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number of locations retrieved</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number of page paginated</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>characted used to filter locations on their label</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>exemplaries successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "exemplaries",
            "description": "<p>list of exemplaries</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_exemplaries",
            "description": "<p>total exemplaries of current location</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_page",
            "description": "<p>total number of page</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "total_returned",
            "description": "<p>number of locations returned</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"exemplaries successfully retrieved\",\n      \"exemplaries\": [\n          {\n              \"label\": null,\n              \"modified\": \"2020-06-01T22:49:23.933Z\",\n              \"created\": \"2020-05-29T18:20:17.575Z\",\n              \"_id\": \"5ed152a623ade50988ff9f9d\",\n              \"location\": {\n                  \"quantity_max\": -1,\n                  \"total_exemplaries\": 2,\n                  \"total_items\": 1,\n                  \"modified\": \"2020-05-29T18:07:41.157Z\",\n                  \"created\": \"2020-05-29T18:07:41.157Z\",\n                  \"_id\": \"5ed1501d897732197421a6e6\",\n                  \"label\": \"etage 4\",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\"\n              },\n              \"item\": {\n                  \"author\": \"Author test update\",\n                  \"editor\": \"Editor test update\",\n                  \"type\": \"Type test update\",\n                  \"date_of_publish\": \"plzoplzo\",\n                  \"total_exemplaries\": 2,\n                  \"path_img\": \"/test\",\n                  \"_id\": \"5ed14ff9897732197421a6e5\",\n                  \"category\": null,\n                  \"label\": \"Label test update \",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\",\n                  \"modified\": \"2020-06-01T22:52:47.582Z\",\n                  \"created\": \"2020-05-29T18:10:01.249Z\"\n              },\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          },\n          {\n              \"label\": null,\n              \"modified\": \"2020-06-01T22:39:25.066Z\",\n              \"created\": \"2020-06-01T22:39:25.066Z\",\n              \"_id\": \"5ed5839e81d5292798e061af\",\n              \"location\": {\n                  \"quantity_max\": -1,\n                  \"total_exemplaries\": 2,\n                  \"total_items\": 1,\n                  \"modified\": \"2020-05-29T18:07:41.157Z\",\n                  \"created\": \"2020-05-29T18:07:41.157Z\",\n                  \"_id\": \"5ed1501d897732197421a6e6\",\n                  \"label\": \"etage 4\",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\"\n              },\n              \"item\": {\n                  \"author\": \"Author test update\",\n                  \"editor\": \"Editor test update\",\n                  \"type\": \"Type test update\",\n                  \"date_of_publish\": \"plzoplzo\",\n                  \"total_exemplaries\": 2,\n                  \"path_img\": \"/test\",\n                  \"_id\": \"5ed14ff9897732197421a6e5\",\n                  \"category\": null,\n                  \"label\": \"Label test update \",\n                  \"library_id\": \"5ed14fc3897732197421a6e3\",\n                  \"modified\": \"2020-06-01T22:52:47.582Z\",\n                  \"created\": \"2020-05-29T18:10:01.249Z\"\n              },\n              \"library_id\": \"5ed14fc3897732197421a6e3\"\n          }\n      ],\n      \"total_exemplaries\": 2,\n      \"total_page\": 1,\n      \"total_returned\": 2\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "LocationNotFound",
            "description": "<p>The <code>id</code> of the Location was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 LocationNotFound\n{\n     \"msg\": \"location not found\"\n     \"location_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during location researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "get",
    "url": "location/:id",
    "title": "Request one",
    "name": "show",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>locations successfully retrieved</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "location",
            "description": "<p>location found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"location found\",\n      \"location\": {\n          \"_id\": \"5ed1501d897732197421a6e6\",\n          \"quantity_max\": -1,\n          \"total_exemplaries\": 1,\n          \"total_items\": 1,\n          \"modified\": \"2020-05-29T18:07:41.157Z\",\n          \"created\": \"2020-05-29T18:07:41.157Z\",\n          \"label\": \"etage 4\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "LocationNotFound",
            "description": "<p>The <code>id</code> of the Location was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 LocationNotFound\n{\n     \"msg\": \"location not found\"\n     \"location_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during location researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "put",
    "url": "location/:id",
    "title": "Update",
    "name": "update",
    "group": "Location",
    "permission": [
      {
        "name": "Token-Full"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of location</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>address of location</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull upgrad</p>"
          },
          {
            "group": "Success 200",
            "type": "Objet",
            "optional": false,
            "field": "location",
            "description": "<p>location object upgraded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull upgrad\",\n      \"location\": {\n          \"quantity_max\": -1,\n          \"total_exemplaries\": 0,\n          \"total_items\": 0,\n          \"modified\": \"2020-06-01T22:44:01.474Z\",\n          \"created\": \"2020-06-01T22:44:01.474Z\",\n          \"_id\": \"5ed584b2d37fc81c5484e9b4\",\n          \"label\": \"etage 4\",\n          \"library_id\": \"5ed14fc3897732197421a6e3\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "LocationNotFound",
            "description": "<p>The <code>id</code> of the Location was not found.</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 LocationNotFound\n{\n     \"msg\": \"location not found\"\n     \"location_id\": 5ed151de16ab5e26109cd068\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\": \"error occured during location creation\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/locationController.ts",
    "groupTitle": "Location"
  },
  {
    "type": "post",
    "url": "/library_connect/:id",
    "title": "LibraryConnect",
    "name": "LibraryConnect",
    "group": "Login",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "description": "<p>Connect to a library is user is member of library. Create and return a token Full if ok.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull connection to library</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "new_token",
            "description": "<p>token full created</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>role of current user in this library</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull connection to library\",\n      \"new_token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTRmOGM4OTc3MzIxOTc0MjFhNmUyIiwibGlicmFyeV9pZCI6IjVlZDE0ZmMzODk3NzMyMTk3NDIxYTZlMyIsImlhdCI6MTU5MTA1NTk3OCwiZXhwIjoxNTkxNjYwNzc4fQ.fbBMlnIQgcq9FFqkqCzr8GPgvZWDRpUm2UeCqKUN_4U\",\n      \"role\": \"admin\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "403": [
          {
            "group": "403",
            "optional": false,
            "field": "NotAutorized",
            "description": "<p>Not autorized to effectuate this action</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action\n{\n     \"msg\": \"this user has no right to connect to this library\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/logginController.ts",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "/library_join",
    "title": "LibraryJoin",
    "name": "LibraryJoin",
    "group": "Login",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "description": "<p>Join a library with the token of library. Permanent added.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token_library",
            "description": "<p>token of library</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull join the new library</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"successfull join the new library\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "ArgumentMissing",
            "description": "<p>Mandatory arguments missing</p>"
          }
        ],
        "403": [
          {
            "group": "403",
            "optional": false,
            "field": "NotAutorized",
            "description": "<p>Not autorized to effectuate this action</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 ArgumentMissing\n{\n     \"msg\": \"mandatory arguments missing. mandatory arguments : email, password\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (403) NotAutorized Not autorized to effectuate this action\n{\n     \"msg\": \"this user has no right to connect to this library\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/logginController.ts",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "login",
    "title": "Login",
    "name": "Login",
    "group": "Login",
    "permission": [
      {
        "name": "Token-Free"
      }
    ],
    "description": "<p>Check credentials. Create and return a token Limited if check is valid.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>last name  of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>first name of user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>successfull connection to library</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token full created</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>current_user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"authentification success\",\n      \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTU2OTk5ZmRkYmQyYTNjZTMwYjBkIiwiaWF0IjoxNTkxMTEyMDMyLCJleHAiOjE1OTE3MTY4MzJ9.TEyfKTzK25gFuZYRbKvsbY0UlTYbYGEDi5HDCNKUbJ4\",\n      \"user\": {\n          \"modified\": \"2020-05-29T18:38:07.731Z\",\n          \"created\": \"2020-05-29T18:38:07.731Z\",\n          \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNWVkMTU2OTk5ZmRkYmQyYTNjZTMwYjBkIiwiaWF0IjoxNTkxMTEyMDMyLCJleHAiOjE1OTE3MTY4MzJ9.TEyfKTzK25gFuZYRbKvsbY0UlTYbYGEDi5HDCNKUbJ4\",\n          \"_id\": \"5ed156999fddbd2a3ce30b0d\",\n          \"email\": \"azerataa@a.com\",\n          \"last_name\": \"eaz\",\n          \"first_name\": \"eaze\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "ArgumentMissing",
            "description": "<p>Mandatory arguments missing</p>"
          }
        ],
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "AuthenticationFailed",
            "description": "<p>AuthenticationFailed</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 ArgumentMissing\n{\n     \"msg\": \"mandatory arguments missing. mandatory arguments : email, password\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 AuthenticationFailed\n{\n     \"msg\": \"authentification failed\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/logginController.ts",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "register",
    "title": "Register",
    "name": "register",
    "group": "Login",
    "permission": [
      {
        "name": "Token-Free"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>last name  of user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>first name of user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>successfully created</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"user successfully created\",\n      \"user\": {\n          \"libraries\": [],\n          \"modified\": \"2020-06-01T23:53:24.336Z\",\n          \"created\": \"2020-06-01T23:53:24.336Z\",\n          \"token\": null,\n          \"_id\": \"5ed5951a04c3d8459847559a\",\n          \"email\": \"compte@test.com\",\n          \"last_name\": \"Bernard\",\n          \"first_name\": \"DuTest\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/logginController.ts",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "media/upload_image",
    "title": "UploadImage",
    "name": "UploadImage",
    "group": "Media",
    "permission": [
      {
        "name": "Token-Limited"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Objet",
            "optional": false,
            "field": "img",
            "description": "<p>the file of image</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>image successfully upload</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "path",
            "description": "<p>path of image uploaded</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"msg\": \"image successfully upload\",\n      \"path_img\": \"https://api-library-ter.herokuapp.com/static/images/c0eb1a26a781019366cd5d12ffae5d93\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "ArgumentMissing",
            "description": "<p>Mandatory arguments missing</p>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>An unknown error occured</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 ArgumentMissing\n{\n     \"msg\": \"no image was passed to the road. mandatory field : img\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 (500) InternalServerError An unknown error occured\n{\n     \"msg\":  \"error occured during exemplary researched\"\n     \"error\": \"error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controller/mediaController.ts",
    "groupTitle": "Media"
  }
] });
